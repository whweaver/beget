# Beget

A ruby dependency management library. It has the following features:
* It is a library, not a framework, and does not have a DSL. Semantics should be idiomatic Ruby.
* Data is monitored for changes based on a hash of its contents, not timestamp.
* Concurrency is the default.
* Many natively supported data types.

Merriam-Webster (as of the time of writing this readme) defines 'beget' as:
1. To procreate as a father
2. To produce especially as an effect or outgrowth

This name hints at the purpose and style of this library, which is to control data which, when processed, begets additional data, which begets additional data, and so on. Both top-down (x produces y, which produces z) and bottom-up (z depends on y, which depends on x) approaches are supported, although the top-down approach feels more natural with the Beget nomenclature. Throughout this document, as well as the Beget API/code, the reproduction/begetting analogy and nomenclature are used.

## Warnings

### Concurrency

Concurrency is the default, although it is largely hidden from the end user. All API methods block by default, and all data yielded to or returned from user-provided blocks is guaranteed to be thread-safe, provided the user does not have other references to this data elsewhere that may be accessed.

HOWEVER, this guarantee does not extend to any memory shared between user-provided blocks through other means. This should be avoided when possible, but if it is unavoidable, it is up to the user to ensure that such sharing is thread-safe.

### Beta

This library relies on [Concurrent Ruby](https://github.com/ruby-concurrency/concurrent-ruby)'s Actor as its core data structure, which is currently in their 'edge' library, indicating a beta development status. While integration testing is done to verify correct operation as much as possible, no guarantee is made concerning its correctness.

## Installation

If using bundle, simply add the following to your Gemfile:

```ruby
gem beget
```

To install globally, run:

```bash
gem install beget
```

## Usage

### Nodes

The central data structure in Beget is the Node. A Node represents a piece of data. Each Node contains information about the data it monitors, how to update it, and when to update it. Beget supports the following data types out of the box, and custom types are easy to write.
```ruby
Beget::Generic
Beget::Object
Beget::File
Beget::Webpage
```

Nodes are monitored for changes by comparing its data's before/after update hash (rather than timestamp, in the case of files, for example).

If you want to monitor some data not adequately covered by these node types, see the [Custom Nodes](#custom-nodes) section below for more information on how to do this.

#### Creating Nodes

```ruby
require 'beget'

object_node = Beget::Object.new(%w[a b c]) do |data|
  # Some code to execute to update the object
end

file_node = Beget::File.new('path/to/file.ext') do |file_pathname|
  # Some code to execute to update the file
end

webpage_node = Beget::Webpage.new('http://www.example.com/', 'path/to/example.html') do
  # Some code to execute to determine if a refresh of the webpage should be done
end
```

### Parents/Children

Each Node may have zero or more parents (from which its data is begotten/produced) and zero or more children (whose data is begotten/produced from this one). If the data contained by one or more of a node's parents is changed, it updates its own data. If its own data has changed, it then processes its children.

You can add parents and children to any node at any time, as illustrated below.

```ruby
# Object Node begets File Node
object_node.beget!(file_node)

# File Node begets Webpage Node
webpage_node.beget_from!(file_node)

# The #beget! and #beget_from! methods return the begotten and begetter, respectively, so chaining is easy.
object_node.beget!(file_node).beget!(webpage_node)
webpage_node.beget_from!(file_node).beget_from!(object_node)
```

Relationships can also be chained with the creation of the nodes. This is a concise and readable way to establish a chain of relationships if most of the nodes do not need to be referenced directly.

```ruby
require 'beget'

webpage_node = Beget::Object.new(%w[a b c]) do |data, *list_of_parents_data|

  # Some code to execute to update the object

end.beget!(

  Beget::File.new('path/to/file.ext') do |file_pathname, *list_of_parents_data|
    # Some code to execute to update the file
  end

).beget!(

  Beget::File.new('http://www.example.com/', 'path/to/example.html') do
    # Some code to execute to determine if a refresh of the webpage should be done
  end

)
```

### Processing

Processing a node causes it to check if any of its parents have changed, and if so, refresh its own data. Options also exist to propagate this processing up and down the begetting chains. The following family tree will be used to illustrate these options.

```
(a)   (b)   (f)
  \   / \   /
   \ /   \ /
   (c)   (g)
   / \
  /   \
(d)   (e)
```

To process the data in a node, simply call its `#process!` method.

```ruby
# Process c node only synchronously.
c.process!

# Process c node asynchronously.
c.process!(:async)

# Process a and b, then c. If a and b had any parents, this would also process those prior to
# a and b, and so on.
c.process!(:parents_first)

# Process c, then d and e. If d and e had any children, this would also process those after d and
# e, and so on.
c.process!(:then_children)

# Process c at a period of 0.1 seconds. This method will never return.
c.process!(periodically: 0.1)

c.process!(periodically: 0.1, until: 10)
```

Note that the above options may be used in any combination as well as individually.

Each node is processed concurrently, if possible. A child will wait for all of its parents to finish processing, and children are processing concurrently to one another, unless one is begotten from another through some other path. However, the `#process!` call is by default synchronous, so when the `#process!` call returns, it is guaranteed that all involved nodes are done processing.

You can also process nodes periodically:

```ruby
# Process c at a period of 0.1 seconds. This method will never return.
c.process!(periodically: 0.1)

# Process c at a period of 0.1 seconds. Returns a controller that can be used to pause or terminate
# the periodic processing.
processor = c.process!(:async, periodically: 0.1)

# Process c at a period of 0.1 seconds until 10 seconds has elapsed.
processor = c.process!(periodically: 0.1, until: 10)
```

<aside class="warning">
No checking is done for circular dependencies. If a node in a cycle is processed with the `:parents_first` option, deadlock will occur. If a node in a cycle is processed with the `:then_children` option, an infinite loop will occur.
</aside>

### NodeLists

Node Lists are variable collections of nodes. They are useful for processing nodes in bulk based on some criteria, or for generating many nodes from one or more others. To create a generic NodeList, you must provide a block that generates the Nodes that will be a part of that NodeList.

NOTE: These generated Nodes are transient: each time the block is run, any Nodes that were present but are no longer present will be dropped from the NodeList and destroyed. All their relationships will also be destroyed.

```ruby
NodeList.new do |*parents_data|
  # Some code that returns an enumerable of Nodes.
  # Will be called each time this Node is refreshed.
end
```

A NodeList is itself a Node, so all the methods that apply to a Node apply to a NodeList. However, a NodeList reacts differently to some of these methods. Children that are added to a NodeList are added to each of the Nodes within the NodeList as well as the NodeList itself.

In order to facilitate the creation of these relationships, #beget! may be provided a block instead of a Node. This block will then be yielded each Node within the NodeList in turn, and the method call will return a NodeList of all Nodes created in this way. This allows NodeLists to beget new generations effortlessly.

To illustrate this, let's create a node to control file "a.txt", which is currently empty.

```ruby
count = 1

a = Beget::File.new('a.txt') do |f|
  f.write(%w[the quick brown fox jumps over the lazy dog][0...count].join("\n"))
  count += 1
end
```

This node can be represented with the following structure.
```
(a)
```

Now, let's process this node.
```ruby
a.process!
```

This puts the following text in "a.txt".
```
the
quick
```

Now, let's have this Node beget a NodeList:

```ruby
nl1 = a.beget!(
  Beget::NodeList.new do |a_file|
    a_file.read.lines.map { |a_line| Beget::Object.new(a_line) }
  end
)
```

The NodeList is created, and runs its provided block once in order to create its initial Nodes. This results in the following structure:

```
    (a)
     |
   (nl1)
     |
  ("the")
```

If we process `a` again, this time with children, we see that it creates another Node.

```ruby
a.process!(:then_children)
```

a.txt
```
the
quick
```

New structure
```
     (a)
      |
    (nl1)
      |
  +---+----+
  |        |
("the") ("quick")
```

Now, let's add another layer to this family.

```ruby
nl2 = nl1.beget! do |nl1_node|
  Beget::NodeList.new do |a_line|
    a_line.chars.map { |a_char| Beget::Object.new(a_char) }
  end
end
```

A new NodeList is created for each Node within nl1, and each one runs its provided block once in order to create its initial Nodes. This results in the following structure:

```
                   (a)
                    |
                  (nl1)
                    |
        +-----------+-----------+
        |                       |
     ("the")                ("quick")
        |                       |
     (the_nl)               (quick_nl)
        |                       |
  +-----+-----+     +-----+-----+-----+-----+
  |     |     |     |     |     |     |     |
("t") ("h") ("e") ("q") ("u") ("i") ("c") ("k")
```

Now, if we process `a` yet again, we see the structure grow again.

```ruby
a.process!(:then_children)
```

a.txt
```
the
quick
brown
```

New structure
```
                               (a)
                                |
                              (nl1)
                                |
        +-----------------------+-----------------------------+
        |                       |                             |
     ("the")                ("quick")                     ("brown")
        |                       |                             |
     (the_nl)               (quick_nl)                    (brown_nl)
        |                       |                             |
 +------+-----+     +-----+-----+-----+-----+     +-----+-----+-----+-----+
 |      |     |     |     |     |     |     |     |     |     |     |     |
("t") ("h") ("e") ("q") ("u") ("i") ("c") ("k") ("b") ("r") ("o") ("w") ("n")
```

If a NodeList shrinks, however, the previous Nodes will be destroyed.

```ruby
count = 1
a.process!(:then_children)
```

a.txt
```
the
```

New structure
```
       (a)
        |
      (nl1)
        |
     ("the") 
        |
     (the_nl)
        |
  +-----+-----+
  |     |     |
("t") ("h") ("e")
```

#### Type-specific NodeLists

Beget also provides several type-specific NodeLists that make creating a NodeList of that Node type even easier.

##### ObjectList

To create an ObjectList, either provide a list of objects as arguments or a block. An ObjectNode will be created for each object in the provided list. If a block is provided, it will be re-evaluated each time the ObjectList is processed.

```ruby
Beget::ObjectList.new(list, of, objects)

Beget::ObjectList.new do
  # Some code that returns an enumerable of objects.
end
```

##### FileList

To create a FileList, either provide a list of glob patterns / regexes or a block. A FileNode will be created for each file that matches ANY of the provided globs or regexes. If a block is provided, it will be re-evaluated each time the FileList is processed.

```ruby
Beget::FileList.new('glob/**/pattern', /regex+/, 'other/glob/*/pattern')

Beget::FileList.new do
  # Some code that returns an enumerable of glob patterns, Pathnames, and/or regexes
end
```

### Caches

It is often desirable to save the state of a set of nodes so they don't have to all be updated the next time the program is run. To make this simple, Beget offers a caching mechanism that can save the state to a file for use later. A cache is contained within a `Beget::Cache` instance, but several conveniences are built into Beget to allow for more efficient caching.

```ruby
# Instantiate a cache
bcache = Beget::Cache.new

# Add a node to the cache using the Node#cache method
node.cache!(bcache)

# Add a node to the cache using the Cache#cache method
bcache.cache!(node)
```

Nodes can also be removed from the cache if desired.

```ruby
# Uncache using the Cache#uncache method
bcache.uncache!(node)

# Uncache the node from all caches it belongs to
node.uncache!

# Uncache the node from bcache only
node.uncache!(bcache)
```

Caches can be created to save to a variety of destinations, including files, arbitrary IO streams, and strings. When the cache is created, it will automatically attempt to load any previously cached data at the location provided.

```ruby
# Cache is saved to a location on disk
Beget::Cache.new('path/to/cache/file')

# Cache is saved to an IO stream
Beget::Cache.new(File.open('...'))

# Cache is saved to a string accessible from its #contents method
Beget::Cache.new
```

Caches by default have the :autosave option set to true. This causes the cache to be updated at its location on disk or elsewhere each time one of the cached nodes updates its data. This option can be disabled if desired.

```ruby
# Do not save the cache whenever the data is updated
bcache = Beget::Cache.new(autosave: false)

# Update the cache with new data and save it to its location
bcache.save
```

By default, caching a node caches only the specified node. However, options exist to automatically cache parents and/or children as well.

```ruby
# Cache any currently existing relationships
node.cache!(bcache, current_parents: true, current_children: true)

# Do NOT cache any currently existing relationships, but automatically add any future children and
# parents to the cache when the relation is created.
node.cache!(bcache, future_parents: true, future_children: true)

# Cache any current and future relationships.
node.cache!(bcache, all_parents: true, all_children: true)

# Cache any current or future node connected to this node through any relationship, directly or
# indirectly, even if the relationship is sibling, cousin, etc.
node.cache!(bcache, network: true)
```

You can also easily add parents or children to a cache when begetting.

```ruby
# Add other_node and node to all caches the other had.
node.beget!(other_node, cache: true)

# Add node to any caches other_node may have.
node.beget!(other_node, cache: :child)

# Add other_node to any caches node may have.
node.beget!(other_node, cache: :parent)
```

### Custom Nodes

To create a custom node, you can either instantiate a `Beget::Generic` node with the appropriate options, or subclass the `Beget::Generic` class, using the template below as a guide.

```ruby
require 'beget/generic'

class Custom < Beget::Generic

  # @param args [Array] The same arguments as Actor#initialize.
  # @param opts [Hash] The same options as Actor#initialize.
  # @yield The same block as Actor#initialize.
  # @return What the name of the node object should be based on the initialization arguments.
  def self.name(*args, **opts, &block)
  end

  private

  # NOTE: It is recommended not to override the initializer for the wrapper class. You should be
  # able to do all your initialization logic in the Actor.

  # @return [Class] The class of the underlying actor object.
  def actor_class
    Actor
  end

  # Each Node has an Actor that does the heavy lifting. The bulk of your node logic should be here.
  class Actor < Beget::Generic::Actor

    private

    # The method to call to refresh the underlying data.
    # @param parents_data [Array] The data of the parent nodes.
    def refresh_data!(*parents_data)
    end

    # @return [Boolean] Whether or not a refresh should be forced, regardless of parents' data.
    def refresh?
    end

    # Initialize whatever internals you want to.
    # @param wrapper [Beget::Generic] The first argument must be the 
    def initialize(wrapper, *args, **opts, &block)
      # Don't forget to call super
      super(wrapper, **opts)
    end

    public

    # @return [String] A unique hash of the data held by this node. Children are refreshed if this
    #   value changes.
    def data_hash
    end

    # @return [Object] The data contained by this actor.
    def data
    end
  end

end
```
