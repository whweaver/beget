# Implements the API and behavior of Mutex EXCEPT if a thread attempts to lock
# the mutex when it was previously locked by the same thread, it maintains the
# lock, rather than raise an error as Mutex does. It maintains a count of the
# number of times the mutex was locked, which is decremented on each unlock,
# ensuring that the mutex is released only when the outermost lock is released.
class RelockableMutex < Mutex

  public

  # Attempts to grab the lock and waits if it isn't available. Does nothing if
  # mutex was previously locked by the current thread.
  def lock
    super unless owned?
    @relockable__lock_count += 1
  end

  # Releases the lock if this is the outermost lock/unlock pair. Raises
  # ThreadError if mutex wasn't locked by the current thread.
  def unlock
    @relockable__lock_count -= 1
    super if @relockable__lock_count == 0
  end

  # Obtains a lock, runs the block, and releases the lock when the block
  # completex. Can be nested. Lock will not be released until the outermost
  # #synchronize block returns.
  def synchronize(&block)
    lock
    begin
      rv = yield
    ensure
      unlock
    end
    rv
  end

  private

  # Initializer.
  def initialize
    super
    @relockable__lock_count = 0
  end
end
