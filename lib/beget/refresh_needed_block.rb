module Beget
  # Provides utilities for handling a @refresh_needed_block
  module RefreshNeededBlock

    private

    # @return [Boolean] True if the node should refresh, false otherwise.
    def refresh?
      if @refresh_needed_block
        if @PASS_PARENTS_AS == :args
          @refresh_needed_block.call(data_as(@INTERPRET_TO_SELF_AS), *parents_data)
        else
          @refresh_needed_block.call(data_as(@INTERPRET_TO_SELF_AS), parents_data)
        end
      else
        false
      end
    end

  end
end
