require 'beget/node'

module Beget

  class Node

    # Shortcut for begetting an Object.
    # @param args [Array] The args to Object.
    # @yield The block to Object.
    # @return [Object] The new Object.
    def beget_object!(*args, &block)
      beget!(Object.new(*args, &block))
    end

  end

  # Represents a Ruby Object and its parents and children
  class Object < Node

    private

    # Initializer.
    # @param obj [Object] The Ruby object to monitor.
    # @param opts [Hash] See Node for additional available options.
    # @yield [obj, parents_data] Yielded to refresh the object.
    # @yieldparam obj [Object] The object monitored by this node.
    # @yieldparam parents_data [Hash] The data of the parents of this node.
    # @yieldreturn [Object] The object monitored by this node.
    def initialize(obj=nil, **opts, &block)

      # Call super
      super(**opts)

      # Load any cached data
      if cache
        # Load the cached data
        begin
          @obj = Marshal.load(cache.load(self)[:data])
        rescue
        end

        # Invalidate the cache for this node if data could not be loaded.
        unless @obj
          cache.invalidate(self)
          reinitialize_state(**opts)
        end
      end

      # Set up instance variables
      @obj ||= obj
      @refresh_block = block if block_given?
    end

    ###########################################################################
    # Internally used methods
    ###########################################################################

    # @return [String] The object's hash.
    def compute_hash
      @obj.hash
    end

    # Refreshes the data in this node.
    # @param data [Object] The data contained by this node.
    # @param parents_data [Hash,Array] Parents' data in the correct format.
    def refresh_data!(data, *parents_data)
      @obj = @refresh_block.call(data, *parents_data) if @refresh_block
    end

    # @return [Object] The data contained by this node.
    def get_data
      @obj
    end

    # @return [Hash] The data to cache.
    def cache_data
      begin
        super.merge!({data: Marshal.dump(@obj)})
      rescue TypeError
        nil
      end
    end

  end
end
