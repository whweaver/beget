require 'pathname'
require 'fileutils'
require 'time'
require 'beget/node'

module Beget

  class Node

    # Shortcut for begetting a File.
    # @param args [Array] The args to File.
    # @yield The block to File.
    # @return [File] The new File.
    def beget_file!(*args, &block)
      beget!(File.new(*args, &block))
    end

  end

  # Represents a file on disk.
  class File < Node

    #########################################################################
    # Accessible within Beget library
    #########################################################################

    # Define the string as the contents of the file.
    # @api private
    # @param format [Symbol] The format to convert to.
    # @return [String] The contents of the file.
    def data_as(format)
      if format == :string
        @pathname.exist? ? @pathname.read : nil
      else
        super
      end
    end

    private

    # Initializer.
    # @param filepath [String] The path to the file to monitor.
    # @param opts [Hash] See Node#initialize for available additional options.
    # @option opts [String] :name
    #   Overridden to the absolute path to the file.
    # @option opts [Time] :last_changed_time
    #   Overridden to the modification time of the file.
    # @option opts [Boolean] :create_file
    #   Whether or not to create the file if it doesn't exist. Defaults to False.
    # @yield [pathname, parents_data] Yielded to refresh the file contents.
    # @yieldparam pathname [Pathname] The pathname to the file.
    # @yieldparam parents_data [Hash] The data of the parents of this node.
    def initialize(filepath, **opts, &block)
      pathname = Pathname.new(filepath).expand_path
      opts[:name] ||= "Beget::File: #{pathname.to_s}"
      opts[:last_changed_time] ||= pathname.mtime if pathname.exist?
      super(**opts)
      @pathname = pathname
      @create_file = opts[:create_file] || false
      @refresh_block = block if block_given?
    end

    # Refreshes the contents of the file by calling the block provided to #initialize.
    # @param data [Object] The data of the node
    # @param parents_data [Hash,Array] The parents data of this node.
    # @return [Boolean] The return value of the block, if provided. False otherwise.
    def refresh_data!(data, *parents_data)
      if @create_file and not @pathname.exist?
        @pathname.dirname.mkpath
        FileUtils.touch(@pathname)
      end
      @refresh_block.call(data, *parents_data) if @refresh_block
    end

    # @return [String] The object's hash.
    def compute_hash
      if @pathname.exist?
        @pathname.read.hash
      else
        nil.hash
      end
    end

    # @return [Pathname] The path to the file.
    def get_data
      @pathname
    end
  end
end
