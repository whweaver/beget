require 'time'
require 'concurrent'
require_relative 'process_cycle'
require_relative 'relockable_mutex'
require_relative 'node/not_passable_error'

# Namespace module for all Beget-related classes.
module Beget

  # Encapsulates some arbitrary data that may depend on other data and may have other data depend
  # on it. Monitors the data and its dependencies for changes and propagates those changes through
  # the dependency network. All method calls are guaranteed to be thread-safe. Should generally not
  # be instantiated directly, but instead subclassed.
  class Node

    # The fields from the node to store in the cache
    CACHED_FIELDS = %i[data_hash last_refreshed_time last_changed_time last_parent_change_time]

    public

    # @return [String] Name of this node.
    def name
      @NAME.clone
    end

    # @return [Boolean] True if the node is external, false otherwise.
    def external?
      @EXTERNAL
    end

    # @return [Time] The time at which this node object was created.
    def start_time
      @START_TIME.clone
    end

    # @return [Concurrent::Executor] The default executor to use for processing this node.
    def executor
      @EXECUTOR
    end

    # @return [Time] The time at which this node's data was last changed.
    def last_changed_time
      @mutex.synchronize do
        @last_changed_time.clone
      end
    end

    # @return [Time] The time at which this node was last refreshed.
    def last_refreshed_time
      @mutex.synchronize do
        @last_refreshed_time.clone
      end
    end

    # @return [Time] The time at which a parent was last added to or removed from this node.
    def last_parent_change_time
      @mutex.synchronize do
        @last_parent_change_time.clone
      end
    end

    # @return [Cache] The cache this node belongs to.
    def cache
      @CACHE
    end

    # @return [String] The last computed hash of the data.
    def data_hash
      @mutex.synchronize do
        (@data_hash ||= compute_hash).clone
      end
    end

    # @return [Hash] The parent nodes of this node, hashed by their names.
    def parents
      @mutex.synchronize do
        @parents.transform_values { |pinfo| pinfo[:node] }
      end
    end

    # @return [Hash] The child nodes of this node, hashed by their names.
    def children
      @mutex.synchronize do
        @children.clone
      end
    end

    # @return [Symbol] How to interpret this node to its children.
    def interpret_to_children_as
      @INTERPRET_TO_CHILDREN_AS
    end

    # @return [Symbol] How to interpret this node to itself.
    def interpret_to_self_as
      @INTERPRET_TO_SELF_AS
    end

    # @return [Symbol] How to interpret this node's parents.
    def interpret_parents_as
      @INTERPRET_PARENTS_AS
    end

    # @return [Symbol] How to pass the parents of this node to its blocks.
    def pass_parents_as
      @PASS_PARENTS_AS
    end

    # @return [Symbol] The format to interpret the specified parent.
    def interpret_parent_as(pname)
      @mutex.synchronize do
        pinfo = @parents[pname]
        pinfo[:opts][:interpret_as] || @INTERPRET_PARENTS_AS || pinfo[:node].interpret_to_children_as
      end
    end

    # Add a child to this node.
    # @param child [Node] The node to make a child of this node.
    # @param opts [Hash] Options controlling execution.
    #   See #add_child_only! and #add_parent_only! for options available only internally to the
    #   beget system.
    # @option opts [Symbol] :interpret_as
    #   How to interpret the parent when passing it to the child for refreshing. Overrides any
    #   :interpret_to_child_as and :interpret_parents_as options the nodes were initialized with.
    #   See #data_as for supported formats.
    # @return [Node] The child
    def beget!(child, **opts)
      @mutex.synchronize do
        child.synchronize do
          add_child_only!(child, **opts)
          child.add_parent_only!(self, **opts)
        end
      end
      child
    end

    # Add a parent to this node.
    # @param parent [Node] The node to make a parent of this node.
    # @param opts [Hash] Options controlling execution.
    #   See #add_child_only! and #add_parent_only! for options available only internally to the
    #   beget system.
    # @option opts [Symbol] :interpret_as
    #   How to interpret the parent when passing it to the child for refreshing. Overrides any
    #   :interpret_to_child_as and :interpret_parents_as options the nodes were initialized with.
    #   See #data_as for supported formats.
    # @return [Node] The parent
    def beget_from!(parent, **opts)
      @mutex.synchronize do
        parent.synchronize do
          add_parent_only!(parent, **opts)
          parent.add_child_only!(self, **opts)
        end
      end
      parent
    end

    # Remove a child from this node.
    # @param child [Node] The node to remove as a child of this node.
    # @param opts [Hash] Options controlling execution. See #add_child_only! and #add_parent_only!
    #   for available options.
    #   WARNING: Should ONLY be used by the Beget system, never by the integrating system.
    # @return [Node] The child
    def unbeget!(child, **opts)
      @mutex.synchronize do
        child.synchronize do
          remove_child_only!(child, **opts)
          child.remove_parent_only!(self, **opts)
        end
      end
      child
    end

    # Remove a parent from this node.
    # @param parent [Node] The node to remove as a parent of this node.
    # @param opts [Hash] Options controlling execution. See #add_child_only! and #add_parent_only!
    #   for available options.
    #   WARNING: Should ONLY be used by the Beget system, never by the integrating system.
    # @return [Node] The parent
    def unbeget_from!(parent, **opts)
      @mutex.synchronize do
        parent.synchronize do
          remove_parent_only!(parent, **opts)
          parent.remove_child_only!(self, **opts)
        end
      end
      parent
    end

    # Initiates a process cycle at the node.
    # @param opts [Hash] Options to control execution.
    # @option opts [Boolean] :parents
    #   Whether or not to process all ancestors before processing this node.
    #   Defaults to false.
    # @option opts [Boolean] :children
    #   Whether or not to process all descendants if this node's data changes
    #   during the process cycle. Defaults to false.
    # @option opts [Boolean] :family
    #   Whether or not to process all relatives of this node when processing
    #   this node. Defaults to false.
    # @option opts [Concurrent::Executor] :executor
    #   The executor to use for this process cycle. Will be used for all nodes
    #   processed as a result of this #process! call and overrides any executor
    #   specified for a node at initialization.
    # @return [ProcessCycle] A cycle representing the processing of all nodes
    #   to be processed by this call.
    def process!(**opts)
      @mutex.synchronize do
        ProcessCycle.new.tap do |pc|
          pc.process_node?(self, **opts)
        end
      end
    end

    # Force a refresh of the data in this node. Does not affect parents or
    # children. Is executed synchronously.
    # @param opts [Hash] Options controlling execution.
    # @option opts [Boolean] :save_cache
    #   Whether or not to save the cache after refreshing. Defaults to true.
    # @return [Boolean] True if the data in this node changed, false otherwise.
    def refresh!(**opts)

      @mutex.synchronize do

        # Make sure a valid hash has been calculated prior to refreshing
        @data_hash ||= compute_hash

        # Refresh the data
        if @PASS_PARENTS_AS == :args
          refresh_data!(data_as(@INTERPRET_TO_SELF_AS), *parents_data)
        else
          refresh_data!(data_as(@INTERPRET_TO_SELF_AS), parents_data)
        end

        # Get current time as close as possible to the actual update time
        @last_refreshed_time = Time.now

        # Calculate the new data hash
        new_hash = compute_hash

        # If a change occured, update the state
        if new_hash != @data_hash
          @data_hash = new_hash
          @last_changed_time = @last_refreshed_time
          rv = true
        else
          rv = false
        end

        # Update the cache.
        update_cache
        @CACHE.commit if @CACHE and opts[:save_cache] != false

        rv
      end
    end

    #########################################################################
    # Accessible within Beget library
    #########################################################################

    # Process this node as part of a process cycle.
    # @api private
    # @param cycle [ProcessCycle] The current processing cycle.
    # @param opts [Hash] Options to control execution. See #process! for the
    #   available options.
    # @return [Future] Future that resolves when this node is done processing and its children
    #   have begun processing (but not finished yet).
    def process_cycle!(cycle, **opts)
      executor = opts[:executor] || @EXECUTOR

      Concurrent::Promises.future_on(executor, opts) do |opts|

        # Process the parents if necessary
        @mutex.synchronize do
          process_parents!(cycle, **opts).value!
        end

      end.then_on(executor) do |parent_changed_times|

        # Refresh if any parent changed
        @mutex.synchronize do
          if should_refresh?(parent_changed_times)
            refresh!(save_cache: false)
          else
            update_cache
            false
          end
        end

      end.then_on(executor, opts) do |was_changed, opts|

        # Process children if requested
        @mutex.synchronize do
          process_children!(cycle, **opts) if opts[:children] or opts[:family]
        end

      end
    end

    # Add a node as a parent to this node.
    # @api private
    # WARNING: Does NOT add the corresponding relationship to the parent.
    # WARNING: Is NOT thread-safe. Must be surrounded by #@mutex.synchronize call.
    # @param parent [Node] The parent node.
    # @param opts [Hash] Options controlling execution. See #beget! and #beget_from! for additional
    #   available options.
    # @option opts [Boolean] :update_parent_change_time
    #   Whether or not to update the last_parent_change_time as a result of this operation.
    #   Defaults to true.
    def add_parent_only!(parent, **opts)
      if @parents[parent.name]
        raise "Node #{name} already has a parent with name #{parent.name}"
      else
        @parents[parent.name] = {node: parent, opts: opts}
        @last_parent_change_time = Time.now if opts[:update_parent_change_time] != false
      end
    end

    # Add a node as a child to this node.
    # @api private
    # WARNING: Does NOT add the corresponding relationship to the child.
    # WARNING: Is NOT thread-safe. Must be surrounded by #@mutex.synchronize call.
    # @param child [Node] The child node.
    # @param opts [Hash] Options controlling execution. None at this time.
    def add_child_only!(child, **opts)
      if @children[child.name]
        raise "Node #{name} already has a child with name #{child.name}"
      else
        @children[child.name] = child
      end
    end

    # Removes the parent from this node.
    # @api private
    # WARNING: Does NOT remove the corresponding relationship from the parent.
    # WARNING: Is NOT thread-safe. Must be surrounded by #@mutex.synchronize call.
    # @param parent [Node] The parent to remove.
    # @param opts [Hash] Options controlling execution.
    # @option opts [Boolean] :update_parent_change_time
    #   Whether or not to update the last_parent_change_time as a result of this operation.
    #   Defaults to true.
    def remove_parent_only!(parent, **opts)
      if @parents[parent.name]
        @parents.delete(parent.name)
        @last_parent_change_time = Time.now if opts[:update_parent_change_time] != false
      else
        raise "Node #{name} has no parent #{parent.name}"
      end
    end

    # Removes the child from this node.
    # @api private
    # WARNING: Does NOT remove the corresponding relationship from the child.
    # WARNING: Is NOT thread-safe. Must be surrounded by #@mutex.synchronize call.
    # @param child [Node] The child to remove.
    # @param opts [Hash] Options controlling execution. None at this time.
    def remove_child_only!(child, **opts)
      if @children[child.name]
        @children.delete(child.name)
      else
        raise "Node #{name} has no child #{child.name}"
      end
    end

    # @api private
    # @return [Object] The data contained by this node.
    def data
      @mutex.synchronize do
        get_data
      end
    end

    # @api private
    # Converts the data contained by this node into the requested format.
    # The standard formats are :node, :none, and :data, which should be
    # supported by all subclasses. All other formats require first converting
    # to the :string format recursively, so subclasses requiring special
    # behavior for string conversion should override this method if :string is
    # passed as a format, and call super for all other formats.
    #
    #   Standard formats:
    #   - node: Passes a reference to the node itself.
    #   - none: Do not pass this node to children.
    #   - data: The node's data in its native format.
    #
    #   String-based formats:
    #   - string: A string representation of the node's data. May not be
    #     available for all subclasses of Node.
    #   - html: Interprets :string formatted data as an HTML document and
    #     provides a Nokogiri HTML object. Requires Nokogiri.
    #   - xml: Interprets :string formatted data as an XML document and
    #     provides a Nokogiri XML object. Requires Nokogiri.
    #   - json: Interprets :string formatted data as JSON and returns a Hash or
    #     Array depending on the JSON data.
    #   - yaml: Interprets :string formatted data as YAML and returns a Hash or
    #     Array depending on the YAML data.
    #   - csv: Interprets :string formatted data as CSV ***without a header***
    #     and returns an Array of Arrays.
    #   - csv_header: Interprets :string formatted data as CSV ***with header***
    #     and returns an Array of Hashes with the headers as symbol keys.
    #   - marshal: Interprets :string formatted data as a Ruby Marshal dump and
    #     returns the unmarshalled object.
    # @return [Object] The data contained by the node in the requested format.
    def data_as(format)
      @mutex.synchronize do
        case format
        when :node
          self
        when :none
          raise NotPassableError
        when :data
          get_data
        when :string
          get_data.to_s
        when :html
          require 'nokogiri'
          Nokogiri::HTML(data_as(:string))
        when :xml
          require 'nokogiri'
          Nokogiri::XML(data_as(:string))
        when :json
          require 'json'
          JSON.load(data_as(:string))
        when :yaml
          require 'yaml'
          YAML.load(data_as(:string))
        when :csv
          data_as(:string).split("\n").map do |line|
            line.split(',').map do |element|
              case element
              when /^-?\d+$/
                element.to_i
              when /^-?(\d*\.\d+|\d+\.\d*)/
                element.to_f
              else
                element
              end
            end
          end
        when :csv_header
          arrays = data_as(:csv)
          headers = arrays[0]
          arrays[1..-1].map { |ary| headers.zip(ary).to_h }
        when :marshal
          Marshal.load(data_as(:string))
        else
          raise "Unknown format '#{format}'"
        end
      end
    end

    # @api private
    # Locks access to the current node.
    def synchronize(&block)
      @mutex.synchronize(&block)
    end

    private

    # Reinitialize state that's cacheable.
    # @param opts [Hash] Options to control execution. See #initialize for details.
    def reinitialize_state(**opts)
      @data_hash = opts[:data_hash]
      @last_changed_time = opts[:last_changed_time]
      if @last_changed_time and @last_changed_time > @START_TIME
        raise "Cannot handle times in the future!"
      end

      @last_refreshed_time = opts[:last_refreshed_time]
      if @last_refreshed_time and @last_refreshed_time > @START_TIME
        raise "Cannot handle times in the future!"
      end

      @last_parent_change_time = opts[:last_parent_change_time]
      if @last_parent_change_time and @last_parent_change_time > @START_TIME
        raise "Cannot handle times in the future!"
      end
    end

    # Initializer.
    # @param opts [Hash] Options for initialization.
    # @option opts [String] :name
    #   The name of the node.
    # @option opts [String] :data_hash
    #   The hash of the data that represents its current state.
    # @option opts [Time] :last_refreshed_time
    #   The time at which the encapsulated data was last updated.
    # @option opts [Enumerable] :parents
    #   The nodes on which this node depends.
    # @option opts [Enumerable] :children
    #   The nodes dependent on this node.
    # @option opts [Boolean] :external
    #   Whether or not this node is updated by something external to the Beget
    #   network. If so, it forces this node to check for data differences every
    #   time it's processed, not just when when a refresh is required. Defaults
    #   to false.
    # @option opts [Concurrent::Executor] :executor
    #   The executor to use for any concurrent processing done by this node.
    #   Can be overridden by providing a :executor option to the #process! call.
    # @option opts [Cache] :cache
    #   The cache this node is stored in. If provided and the node has
    #   previously been saved in the cache, the cached data will override any
    #   provided state options, including the last_*_times and the data hash.
    # @option opts [Symbol] :interpret_to_children_as
    #   How to pass this node to its children when the children are refreshing.
    #   See #data_as for supported formats. Defaults to :data
    # @option opts [Symbol] :interpret_to_self_as
    #   How to pass this node to itself when it is refreshing. See #data_as for
    #   supported formats. Defaults to :data
    # @option opts [Symbol] :interpret_as
    #   Specifies both :interpret_to_children_as and :interpret_to_self_as with
    #   one option.
    # @option opts [Symbol] :interpret_parents_as
    #   How to interpret the parents of this node when it is refreshing. Will
    #   override the parents' :interpret_to_children_as setting for this node
    #   only. See #data_as for supported formats.
    # @option opts [Symbol] :pass_parents_as
    #   How to pass the parents of this node to any associated block. Possible
    #   values are:
    #   - args: Parents' data, in the format specified by the :interpret_as
    #     options, are passed to the block as arguments to the block.
    #   - hash: parents' data, in the format specified by the :interpret_as
    #     options, are passed to the block as a hash with the parents' names
    #     as keys.
    def initialize(**opts)
      @START_TIME = Time.now
      @NAME = opts[:name] || "#{self.class}: #{object_id.to_s(16)}"
      @EXTERNAL = opts[:external] || false
      @EXECUTOR = opts[:executor] || Concurrent.global_io_executor
      @mutex = RelockableMutex.new
      @parents = {}
      @children = {}
      @CACHE = opts[:cache]
      @INTERPRET_TO_CHILDREN_AS = opts[:interpret_to_children_as] || opts[:interpret_as] || :data
      @INTERPRET_TO_SELF_AS = opts[:interpret_to_self_as] || opts[:interpret_as] || :data
      @INTERPRET_PARENTS_AS = opts[:interpret_parents_as] || nil
      @PASS_PARENTS_AS = opts[:pass_parents_as] || :args

      # Override any opts from the provided cache.
      opts.merge!(@CACHE.load(self)) if @CACHE

      # Do the state initialization (for the first time)
      reinitialize_state(**opts)

      opts[:parents].each { |pn| beget_from!(pn, update_parent_change_time: false) } if opts[:parents]
      opts[:children].each { |cn| beget!(cn, update_parent_change_time: false) } if opts[:children]
    end

    # Determine if this node should refresh.
    # Refresh if:
    # - This node can be modified by something external to the Beget system.
    # - This node has never been refreshed.
    # - At least one parent has never been changed and this node hasn't refreshed yet.
    # - At least one parent has been added or removed since this node as last refreshed.
    # - At least one parent was changed more recently than this one was.
    # - A refresh is requested.
    # @param parent_changed_times [Array<Time>] The last changed times of all parents.
    # @return [Boolean] True if this node should refresh, false otherwise.
    def should_refresh?(parent_changed_times)
      most_recent_parent_change = parent_changed_times.compact.max

      @EXTERNAL or
        @last_refreshed_time.nil? or
        ((@last_refreshed_time < @START_TIME) and (parent_changed_times.count(&:nil?) > 0)) or
        (@last_parent_change_time and (@last_parent_change_time > @last_refreshed_time)) or
        (most_recent_parent_change and (most_recent_parent_change > @last_refreshed_time)) or
        refresh?
    end

    # Process the parents of this node.
    # @param cycle [ProcessCycle] The current processing cycle.
    # @param opts [Hash] The options passed to this process call.
    # @return [Future] Future that resolves to the last changed time of all parents.
    def process_parents!(cycle, **opts)
      parent_opts = opts.reject { |k, v| k == :children }

      if opts[:parents] or opts[:family]

        futures = @parents.map do |pname, pinfo|
          pnode = pinfo[:node]
          cycle.process_node?(pnode, **parent_opts).then_on(opts[:executor] || pnode.executor, pnode) do |v, pnode|
            pnode.last_changed_time
          end
        end

      else
        futures = @parents.map do |pname, pinfo|
          pnode = pinfo[:node]
          Concurrent::Promises.future_on(opts[:executor] || pnode.executor, pnode) do |pnode|
            pnode.last_changed_time
          end
        end

      end

      Concurrent::Promises.zip_futures_on(opts[:executor] || @EXECUTOR, *futures)
    end

    # Process the children of this node. Starts the processing asynchronously
    # and then returns.
    # @param cycle [ProcessCycle] The current processing cycle.
    # @param opts [Hash] The options passed to this process call.
    def process_children!(cycle, **opts)
      child_opts = opts.reject { |k, v| k == :parents }

      @children.each do |cname, cnode|
        cycle.process_node?(cnode, **child_opts)
      end
    end

    # @return [Hash] The data of the parents.
    def parents_data
      pdata_hash = {}
      @parents.each do |pname, pinfo|
        begin
          pdata_hash[pname] = pinfo[:node].data_as(interpret_parent_as(pname))
        rescue NotPassableError
          # Don't add unpassable nodes
        end
      end
      @PASS_PARENTS_AS == :args ? pdata_hash.values : pdata_hash
    end

    # Update this node's cache.
    def update_cache
      @CACHE.store(self, cache_data) if @CACHE
    end

    ###########################################################################
    # Overrideable methods
    ###########################################################################

    # Refreshes the data in this node.
    # Should be overridden by implementing subclass.
    # @param data [Object] The data contained by this node in the correct format
    # @param parents_data [Hash,Array] Each parent's data in the correct format
    def refresh_data!(data, *parents_data)
    end

    # Should be overridden by implementing subclass.
    # @return [Boolean] True if a refresh should be forced, false otherwise.
    def refresh?
      false
    end

    # Should be overridden by implementing subclass.
    # @return [String] A unique hash of the data held by this node.
    def compute_hash
      get_data.hash
    end

    # Should be overridden by implementing subclass.
    # @return [Object] The data contained by this node.
    def get_data
      nil
    end

    # Can be overridden by implementing subclass, but additional fields should
    # be merged with these by calling super.
    # @return [Hash] The data to cache.
    def cache_data
      CACHED_FIELDS.map do |field|
        [field, send(field)]
      end.to_h
    end

  end
end
