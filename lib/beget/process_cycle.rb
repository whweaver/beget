require 'concurrent'

module Beget

  # A processing cycle of the entire dependency network.
  class ProcessCycle

    public

    # @return [Boolean] True if the cycle is completely done processing, false otherwise.
    def done?
      @done_event.set?
    end

    # Wait for a given number of seconds for the cycle to be completely done processing.
    # @param timeout [Numeric] The number of seconds to wait before returning. If nil, wait forever.
    def wait(timeout=nil)
      @done_event.wait(timeout)
    end

    # @return [Hash] The nodes and their process futures that were rejected.
    def rejections
      @mutex.synchronize do
        @futures.select { |n, f| f.rejected? }.merge(@done_future ? {nil => @done_future} : {})
      end
    end

    # Raises the rejected futures.
    def raise_rejections
      Concurrent::Promises.zip_futures_on(:fast, *rejections.values).value!
    end

    #########################################################################
    # Accessible within Beget library
    #########################################################################

    # Processes the node if it has not been processed by the cycle.
    # @param node [Node] Node to process.
    # @param opts [Hash] The options to pass to Node#process_cycle
    # @return [Future] Future that fulfills when the node is done processing.
    def process_node?(node, **opts)
      executor = opts[:executor] || node.executor

      @mutex.synchronize do
        
        # Start processing the node if it's not already started
        unless @futures[node]

          # Start processing the node
          @futures[node] ||= node.process_cycle!(self, **opts)

          # Chain the finishing sequence onto the future.
          @futures[node].on_rejection_using(executor, node) do |reason, node|
            finish_processing(node)
          end.then_on(executor, node) do |node|
            finish_processing(node)
          end
        end

        @futures[node]
      end
    end

    private

    # Initializer.
    def initialize
      @futures = {}
      @mutex = Mutex.new
      @done_event = Concurrent::Event.new
    end

    # Check if the cycle is done. If so, trigger the done event.
    # A cycle is done if no nodes are currently processing. Since each node will start the
    # processing for its own parents and children if necessary, when no nodes are processing,
    # we know there will be no more nodes to process in this cycle, and thus we can end it.
    # @param node [Node] The node that's finishing processing.
    def finish_processing(node)
      @mutex.synchronize do
        # Check to see if no other nodes are processing.
        if @futures.values.count { |v| not v.resolved? } == 0

          # Save all relevant caches in separate threads.
          saved_caches = Set.new
          cache_save_futures = @futures.map do |node, future|
            cache = node.cache
            future.then_on(:io) { cache.save } if cache and saved_caches.add?(cache)
          end.compact

          # Alert main thread that this cycle is done once caches are done being saved.
          zipped_cache_saves = Concurrent::Promises.zip_futures_on(:fast, *cache_save_futures)
          @done_future = zipped_cache_saves.then_on(:fast) do
            @done_event.set
          end
        end
      end
    end

  end
end
