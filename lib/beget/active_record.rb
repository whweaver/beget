require 'beget/node'

module Beget

  class Node

    # Shortcut for begetting an ActiveRecord.
    # @param args [Array] The args to ActiveRecord.
    # @yield The block to ActiveRecord.
    # @return [Object] The new ActiveRecord.
    def beget_active_record!(*args, &block)
      beget!(ActiveRecord.new(*args, &block))
    end

  end

  # Represents a database entry.
  class ActiveRecord < Node

    private

    # Initializer.
    # @param record [ActiveRecord] The ActiveRecord to monitor.
    # @param opts [Hash] See Node#initialize for available additional options.
    # @option opts [String] :name
    #   Overridden to include the type of the record and its primary keys.
    # @option opts [Time] :last_changed_time
    #   Overridden to record.updated_at if it exists.
    # @yield [record, parents_data] Yielded refresh the record contents.
    # @yieldparam record [ActiveRecord] The record being monitored.
    # @yieldparam parents_data [Hash] The data of the parents of this node.
    def initialize(record, **opts, &block)
      require 'active_record'

      # Calculate name for this record
      unless opts[:name]
        pks = ::ActiveRecord::Base.connection.primary_key(record.class.table_name)
        pks = [pks] if pks.is_a?(String) or pks.is_a?(Symbol)
        pk_vals = pks.map { |pk| record.public_send(pk) }
        pk_str = pk_vals.join(',')
        opts[:name] = "#{self.class.to_s}::#{record.class.to_s}: #{pk_str}"
      end

      # Override last changed time if available
      opts[:last_changed_time] ||= record.updated_at if record.respond_to?(:updated_at)

      # Call super
      super(**opts)

      # Initialize state
      @record = record
      @refresh_block = block if block_given?
    end

    # Refreshes the data in this node. Calls the block provided to #initialize.
    # @param record [ActiveRecord] The record contained by this node.
    # @param parents_data [Hash] Parents' data in the correct format.
    def refresh_data!(record, *parents_data)
      @refresh_block.call(record, *parents_data) if @refresh_block
    end

    # @return [String] Hash of the attribute hash
    def compute_hash
      @record.attributes.hash
    end

    # @return [ActiveRecord] The path to the file.
    def get_data
      @record
    end

    # Override to always raise when converting to string.
    # @param format [Symbol] The format to convert to.
    def data_as(format)
      raise "Cannot convert ActiveRecord to string!" if format == :string
      super
    end
  end
end
