require 'open-uri'
require 'beget/object'
require 'beget/refresh_needed_block'

module Beget
  class Node

    # Shortcut for begetting a Webpage.
    # @param args [Array] The args to Webpage.
    # @yield The block to Webpage.
    # @return [Webpage] The new Webpage.
    def beget_object_webpage!(*args, &block)
      beget!(Object::Webpage.new(*args, &block))
    end

  end

  class Object

    # Represents a downloaded webpage and its parents and children
    class Webpage < Object

      include RefreshNeededBlock

      private

      # Initializer.
      # @param uri [String] The URL to the webpage to download.
      # @param opts [Hash] See Object for available additional options.
      # @option opts [Array] :open_args
      #   Additional arguments to pass to URI.open
      # @yield Returns whether or not to refresh the webpage.
      def initialize(uri, **opts, &block)
        super(**opts) do |obj, *args|
          URI.open(URI.escape(uri), *(opts[:open_args] || [])) do |w|
            w.read
          end
        end
        @refresh_needed_block = block if block_given?
      end

    end
  end
end
