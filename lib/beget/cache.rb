require 'yaml'
require 'fileutils'

module Beget
  # A cache of Beget Node state.
  class Cache

    public

    # Get the stored data for the specified node.
    # @param node [Node] The node to retrieve the data for.
    # @return [Object] The data.
    def load(node)
      state[node.name] || {}
    end

    # Update the current node in the cache. Will not store nil as the data.
    # @param node [Node] The node to update.
    # @param data [#to_yaml] The data to store in the cache. Must respond to #to_yaml.
    def store(node, data)
      state[node.name] = data if data
    end

    # Remove the specified node's data from the cache.
    # @param node [Node] The node to invalidate.
    def invalidate(node)
      state.delete(node.name)
    end

    # Commit the cached state to the storage medium.
    def commit

      # Only commit if the state string has changed (compare using a hash).
      state_str = @state.to_yaml
      new_save_hash = state_str.hash
      if new_save_hash != @last_saved_hash

        # Write to an IO object
        if @medium.respond_to?(:write)
          @medium.truncate(0)
          @medium.write(state_str)

        # Write to the specified file
        else
          FileUtils.mkdir_p(::File.dirname(@medium))
          ::File.write(@medium, state_str)
        end
      end
    end

    private

    # @return [Hash] The cached state.
    def state

      # @state is memoized.
      unless @state

        # Read from an IO object
        if @medium.respond_to?(:read)
          state_str = @medium.read

        # Read from the specified file
        else
          state_str = ::File.exist?(@medium) ? ::File.read(@medium) : ''
        end

        # Initialize state information
        @last_saved_hash = state_str.hash
        if state_str.empty?
          @state = {}
        else
          @state = YAML.load(state_str)
        end
      end

      # Return the state
      @state
    end

    # Create a cache instance.
    # @param medium [String, IO] If a string, the path to the file containing the cache. If IO,
    #   and open readable and writeable IO object that functions as the medium for the cache.
    def initialize(medium)
      @nodes = {}
      @medium = medium
    end

  end
end
