require 'open-uri'
require 'beget/file'
require 'beget/refresh_needed_block'

module Beget
  class Node

    # Shortcut for begetting a Webpage.
    # @param args [Array] The args to Webpage.
    # @yield The block to Webpage.
    # @return [Webpage] The new Webpage.
    def beget_file_webpage!(*args, &block)
      beget!(File::Webpage.new(*args, &block))
    end

  end

  class File

    # Represents a downloaded webpage and its parents and children
    class Webpage < File

      include RefreshNeededBlock

      private

      # Initializer.
      # @param uri [String] The URL to the webpage to download.
      # @param filepath [String] The path to the download location.
      # @param opts [Hash] See File for available additional options.
      # @option opts [Boolean] :create_file
      #   Overridden to be true always.
      # @option opts [Array] :open_args
      #   Additional arguments to pass to URI.open
      # @yield Returns whether or not to refresh the webpage.
      def initialize(uri, filepath, **opts, &block)
        super(filepath, **opts.merge({create_file: true})) do |pathname, *args|
          URI.open(URI.escape(uri), *(opts[:open_args] || [])) do |w|
            @pathname.write(w.read, mode: 'wb')
          end
        end
        @refresh_needed_block = block if block_given?
      end

    end
  end
end
