raise "Must be on jruby" unless RUBY_PLATFORM.include?('java')

require "bundler"
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  raise LoadError.new("Unable to Bundler.setup(): You probably need to run `bundle install`: #{e.message}")
end

require "rspec"
require "rspec/core/rake_task"
require "yard"
require "simplecov"

RSpec::Core::RakeTask.new(:spec, :example_string) do |task, args|
  if args.example_string
    ENV["partial_specs"] = "1"
    task.rspec_opts = %W[-e "#{args.example_string}" -f documentation]
  end
end

YARD::Rake::YardocTask.new do |yard|
  yard.options = ["--private", "--title", "beget gem"]
  yard.files = ["lib/**/*.rb"]
end
# Display undocumented items.
task :yard do
  yard_status_output = IO.popen(%w[yard stats --private --list-undoc]) do |io|
    io.read
  end
  if undocumented = yard_status_output[/Undocumented Objects:.*/m]
    $stderr.puts undocumented
  end
end
