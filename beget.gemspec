Gem::Specification.new do |s|
  s.name        = 'beget'
  s.version     = '0.1.0'
  s.licenses    = ['GPLv3']
  s.summary     = "Rake-inspired task/file management library."
  s.description = <<-HEREDOC
Beget is a data dependency management library. It is designed to manage the
flow of data through an application.

Merriam-Webster defines 'beget' as:
1. To procreate as a father
2. To produce especially as an effect or outgrowth

The Beget gem controls data that, when processed, produces other data. A
network of such production and dependencies can be constructed such that, when
processed, only those pieces of the chain that need to be refreshed are
actually refreshed.

To give an example, this gem was originally developed to assist in web
scraping. An index page is downloaded, and pieces of it are inserted into a
database. Based on those contents, additional pages are downloaded and their
contents catalogued correspondingly in order to build a database of the
contents of the entire site.

To manage such an application, a network of webpages and the database entries
produced or updated by them is constructed using Beget. Then, when the database
needs to be updated from the scraped site, Beget automatically updates only
those pages that need to be updated, and computes only those database entries
that need to be updated.

This is remiscent of some of the functionality of rake. This library was indeed
built as a replacement for rake within the original target application, but it
is not a direct competitor. It intends to perform rake-like dependency
management within the context of a larger application, while rake is intended
to be run outside of the context of an application or even to control the
application itself. A comparison is included below for the purposes of better
defining what Beget is, and what it isn't.

Like rake, Beget...
- Monitors files for changes and executes code when changes occur.

Unlike rake, Beget...
- Is a library to be used within another application, not a command-line tool.
- Uses a data-oriented paradigm rather than a task-oriented paradigm.
- Can manage arbitrary ruby objects, even across power cycles.
- Can manage database entries (through ActiveRecord).
- Lends itself more naturally to a top-down approach (X produces Y produces Z)
  rather than a bottom-up approach (Z depends on Y depends on X), although both
  are possible.
- Easily creates additional data nodes dynamically (dynamic task creation can
  be clumsy in rake).
- Tracks state using hashes and a record of most recent activity to ensure no
  data is unnecessarily refreshed.
- Can cache state on disk to prevent unnecessary refreshes after a power or
  invocation cycle.

See the README at the Homepage Link for more details and examples.
HEREDOC
  s.authors     = ["Will Weaver"]
  s.email       = nil
  s.files       = Dir.glob("lib/**/*")
  s.homepage    = 'https://gitlab.com/whweaver/beget'

  s.add_development_dependency 'rspec', '~>3.9'
  s.add_development_dependency "simplecov", "~> 0.9"
  s.add_development_dependency "yard", "~> 0.8"
  s.add_development_dependency "rake", ">= 13"
  s.add_development_dependency "activerecord", "~> 5.0"
  s.add_development_dependency "activerecord-jdbcsqlite3-adapter"
  s.add_development_dependency "nokogiri", "~> 1.10"
  s.add_runtime_dependency 'concurrent-ruby', '~> 1.1'
end
