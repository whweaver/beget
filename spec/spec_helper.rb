require "simplecov"

#require all of the support files!
Dir[File.dirname(__FILE__) + "/support/**/*.rb"].each {|f| require f}

SimpleCov.start do
  add_filter "/spec/"
  if ENV["partial_specs"]
    command_name "RSpec-partial"
  else
    command_name "RSpec"
  end
end

TEST_DIRECTORY = 'Build/spec'
