require 'spec_helper'
require 'beget'
require 'open-uri'

module Beget
  class Object
    describe Node do

      describe '#beget_object_webpage!' do

        it "begets a Webpage" do
          # create test state/variables
          n = Node.new

          # mocks/stubs/expected calls
          # execute method
          n.beget_object_webpage!('http://example.com/', name: 'ow') do
            Node.new(name: 'b')
          end

          # validate results
          expect(n.children.keys).to eq %w[ow]
        end

      end

    end

    describe Webpage do

      describe '#initialize' do

        it "initializes" do
          # create test state/variables
          # mocks/stubs/expected calls
          # execute method
          w = Webpage.new('http://example.com/')

          # validate results
          expect(w.name).to start_with "Beget::Object::Webpage: "
          expect(w.last_refreshed_time).to be_nil
          expect(w.last_changed_time).to be_nil
        end

      end

      describe '#process!' do

        it "refreshes if provided block returns true" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          interpret_to_self_as: :html,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10) do |html|
            expect(html.to_html).to eq "" # Has not been downloaded yet
            refreshed_times += 1
            true
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time
          webpage_contents = nil
          open(URI.escape('http://example.com/')) do |w|
            webpage_contents = w.read
          end

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          pc.raise_rejections
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.data).to eq webpage_contents
          expect(w.last_refreshed_time).to be > prev_lrt
          expect(w.last_changed_time).to be > prev_lct
        end

        it "does not refresh if provided block returns false" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          pass_parents_as: :hash,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10) do |data, parents_data|
            expect(parents_data).to eq({})
            expect(data).to eq nil # Has not been downloaded yet
            refreshed_times += 1
            false
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.last_refreshed_time).to eq prev_lrt
          expect(w.last_changed_time).to eq prev_lct
        end

        it "does not refresh if no block is provided" do
          # create test state/variables
          w = Webpage.new('http://example.com/',
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10)
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(w.last_refreshed_time).to eq prev_lrt
          expect(w.last_changed_time).to eq prev_lct
        end

        it "opens with provided args if provided" do
          # create test state/variables
          w = Webpage.new('http://example.com/',
                          open_args: [{ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}]) { true }

          # mocks/stubs/expected calls
          expect(URI).to receive(:open).with('http://example.com/', {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE})

          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
        end

        it "refreshes with html" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10,
                          interpret_as: :html) do |html|
            expect(html).to be_a Nokogiri::HTML::Document
            refreshed_times += 1
            true
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time
          webpage_contents = nil
          open(URI.escape('http://example.com/')) do |w|
            webpage_contents = w.read
          end

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.data).to eq webpage_contents
          expect(w.last_refreshed_time).to be > prev_lrt
          expect(w.last_changed_time).to be > prev_lct
        end

      end

    end
  end
end
