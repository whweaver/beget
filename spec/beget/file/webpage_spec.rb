require 'spec_helper'
require 'beget'
require 'nokogiri'

module Beget
  class File
    describe Node do

      describe '#beget_file_webpage!' do

        it "begets a Webpage" do
          # create test state/variables
          n = Node.new

          # mocks/stubs/expected calls
          # execute method
          n.beget_file_webpage!('http://example.com/', 'path/to/file', name: 'fw') do
            Node.new(name: 'b')
          end

          # validate results
          expect(n.children.keys).to eq %w[fw]
        end

      end

    end

    describe Webpage do

      TEST_WEBPAGE_DIR = ::File.join(TEST_DIRECTORY, 'Webpage')
      TEST_WEBPAGE_FILE = ::File.join(TEST_WEBPAGE_DIR, 'a.tmp')

      before :each do
        FileUtils.rm_rf(TEST_WEBPAGE_DIR)
        FileUtils.mkdir_p(TEST_WEBPAGE_DIR)
      end

      describe '#initialize' do

        it "initializes" do
          # create test state/variables
          # mocks/stubs/expected calls
          # execute method
          w = Webpage.new('http://example.com/', TEST_WEBPAGE_FILE)

          # validate results
          expect(w.name).to eq "Beget::File: #{::File.expand_path(TEST_WEBPAGE_FILE)}"
          expect(w.last_refreshed_time).to be_nil
          expect(w.last_changed_time).to be_nil
        end

      end

      describe '#process!' do

        it "refreshes if provided block returns true" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          TEST_WEBPAGE_FILE,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10) do |data|
            expect(data.expand_path).to eq Pathname.new(TEST_WEBPAGE_FILE).expand_path
            refreshed_times += 1
            true
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time
          webpage_contents = nil
          open(URI.escape('http://example.com/')) do |w|
            webpage_contents = w.read
          end

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.data.read).to eq webpage_contents
          expect(w.last_refreshed_time).to be > prev_lrt
          expect(w.last_changed_time).to be > prev_lct
        end

        it "does not refresh if provided block returns false" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          TEST_WEBPAGE_FILE,
                          pass_parents_as: :hash,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10) do |data, parents_data|
            expect(parents_data).to eq({})
            expect(data.expand_path).to eq Pathname.new(TEST_WEBPAGE_FILE).expand_path
            refreshed_times += 1
            false
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.last_refreshed_time).to eq prev_lrt
          expect(w.last_changed_time).to eq prev_lct
        end

        it "does not refresh if no block is provided" do
          # create test state/variables
          w = Webpage.new('http://example.com/',
                          TEST_WEBPAGE_FILE,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10)
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(w.last_refreshed_time).to eq prev_lrt
          expect(w.last_changed_time).to eq prev_lct
        end

        it "opens with provided args if provided" do
          # create test state/variables
          w = Webpage.new('http://example.com/',
                          TEST_WEBPAGE_FILE,
                          open_args: [{ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}])

          # mocks/stubs/expected calls
          expect(URI).to receive(:open).with('http://example.com/', {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE})

          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
        end

        it "refreshes with html" do
          # create test state/variables
          refreshed_times = 0
          w = Webpage.new('http://example.com/',
                          TEST_WEBPAGE_FILE,
                          last_refreshed_time: Time.now - 1,
                          last_changed_time: Time.now - 10,
                          interpret_as: :html) do |html|
            expect(html).to be_a Nokogiri::HTML::Document
            refreshed_times += 1
            true
          end
          prev_lrt = w.last_refreshed_time
          prev_lct = w.last_changed_time
          webpage_contents = nil
          open(URI.escape('http://example.com/')) do |w|
            webpage_contents = w.read
          end

          # mocks/stubs/expected calls
          # execute method
          pc = w.process!
          pc.wait

          # validate results
          expect { pc.raise_rejections }.to_not raise_error
          expect(refreshed_times).to be > 0
          expect(w.data.read).to eq webpage_contents
          expect(w.last_refreshed_time).to be > prev_lrt
          expect(w.last_changed_time).to be > prev_lct
        end

      end

    end
  end
end
