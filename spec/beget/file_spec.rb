require 'spec_helper'
require 'beget'

module Beget
  describe Node do

    describe '#beget_file!' do

      it "begets a File" do
        # create test state/variables
        n = Node.new

        # mocks/stubs/expected calls
        # execute method
        n.beget_file!('path/to/file', name: 'f') do
          Node.new(name: 'b')
        end

        # validate results
        expect(n.children.keys).to eq %w[f]
      end

    end

  end

  describe File do

    TEST_EXTANT_DIR = ::File.join(TEST_DIRECTORY, 'File', 'extant')
    TEST_EXTANT_FILE = ::File.join(TEST_EXTANT_DIR, 'a.tmp')
    TEST_NONEXISTENT_DIR = ::File.join(TEST_DIRECTORY, 'File', 'nonexistent')
    TEST_NONEXISTENT_FILE = ::File.join(TEST_NONEXISTENT_DIR, 'a.tmp')

    before :each do
      FileUtils.rm_rf(TEST_NONEXISTENT_DIR)
      FileUtils.mkdir_p(TEST_EXTANT_DIR)
      FileUtils.rm_rf(TEST_EXTANT_FILE)
      FileUtils.touch(TEST_EXTANT_FILE)
    end

    describe '#initialize' do

      it "initializes with file that exists" do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        f = File.new(TEST_EXTANT_FILE)

        # validate results
        expect(f.name).to eq "Beget::File: #{::File.expand_path(TEST_EXTANT_FILE)}"
        expect(f.last_changed_time).to eq ::File.mtime(TEST_EXTANT_FILE)
        expect(f.last_refreshed_time).to be_nil
      end

      it "initializes with file that does not exist" do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        f = File.new(TEST_NONEXISTENT_FILE)

        # validate results
        expect(f.name).to eq "Beget::File: #{::File.expand_path(TEST_NONEXISTENT_FILE)}"
        expect(f.last_changed_time).to be_nil
        expect(f.last_refreshed_time).to be_nil
      end

    end

    describe '#data' do

      it "returns the absolute pathname" do
        # create test state/variables
        f = File.new(TEST_EXTANT_FILE)

        # mocks/stubs/expected calls
        # execute method
        data = f.data

        # validate results
        expect(data).to eq Pathname.new(TEST_EXTANT_FILE).expand_path
      end

    end

    describe '#refresh!' do

      it "creates the file if create_file was specified" do
        # create test state/variables
        f = File.new(TEST_NONEXISTENT_FILE, create_file: true)
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        f.refresh!

        # validate results
        expect(::File.exist?(TEST_NONEXISTENT_FILE)).to be_truthy
        expect(f.last_refreshed_time).to be > prev_time
        expect(f.last_changed_time).to be > prev_time
      end

      it "does not create the file if create_file was not specified" do
        # create test state/variables
        f = File.new(TEST_NONEXISTENT_FILE)
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        f.refresh!

        # validate results
        expect(::File.exist?(TEST_NONEXISTENT_FILE)).to be_falsey
        expect(f.last_refreshed_time).to be > prev_time
        expect(f.last_changed_time).to be_nil
      end

      it "executes the update block" do
        # create test state/variables
        f = File.new(TEST_EXTANT_FILE) do |pathname|
          pathname.open('wb') do |file|
            file.write("Something\n")
          end
        end
        last_changed_time = f.last_changed_time

        # mocks/stubs/expected calls
        # execute method
        f.refresh!

        # validate results
        expect(f.data.read).to eq "Something\n"
        expect(f.last_refreshed_time).to eq f.last_changed_time
        expect(f.last_changed_time).to be > last_changed_time
      end

      it "executes the update block with extant data as string" do
        # create test state/variables
        read_contents = nil
        f = File.new(TEST_EXTANT_FILE, interpret_as: :string) do |contents|
          read_contents = contents
        end

        # mocks/stubs/expected calls
        # execute method
        f.refresh!

        # validate results
        expect(read_contents).to eq ""
      end

      it "executes the update block with nonexistent data as string" do
        # create test state/variables
        read_contents = ""
        f = File.new(TEST_NONEXISTENT_FILE, interpret_as: :string) do |contents|
          read_contents = contents
        end

        # mocks/stubs/expected calls
        # execute method
        f.refresh!

        # validate results
        expect(read_contents).to eq nil
      end

      it "executes the update block with parents data" do
        # create test state/variables
        f1 = File.new(TEST_EXTANT_FILE, interpret_to_children_as: :string) do |pathname|
          pathname.open('wb') do |file|
            file.write("Something\n")
          end
        end
        f2 = File.new(TEST_NONEXISTENT_FILE, create_file: true) do |pathname, f1_data|
          expect(f1_data).to eq "Something\n"
          pathname.open('wb') do |file|
            file.write("Something else\n")
          end
        end
        f1.beget!(f2)

        # mocks/stubs/expected calls
        # execute method
        pc = f2.process!(parents: true)
        pc.wait

        # validate results
        expect{pc.raise_rejections}.to_not raise_error
        expect(f1.data.read).to eq "Something\n"
        expect(f2.data.read).to eq "Something else\n"
      end

    end

  end
end
