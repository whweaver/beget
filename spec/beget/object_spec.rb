require 'spec_helper'
require 'beget'

module Beget
  describe Node do

    describe '#beget_object!' do

      it "begets an Object" do
        # create test state/variables
        n = Node.new

        # mocks/stubs/expected calls
        # execute method
        n.beget_object!('object', name: 'o') do
          Node.new(name: 'b')
        end

        # validate results
        expect(n.children.keys).to eq %w[o]
      end

    end

  end

  describe Object do

    describe '#initialize' do

      it "initializes with an object" do
        # create test state/variables
        obj = "Random object"
        # mocks/stubs/expected calls
        # execute method
        o = Object.new(obj)

        # validate results
        expect(o.name).to eq "Beget::Object: #{o.object_id.to_s(16)}"
        expect(o.last_refreshed_time).to be_nil
        expect(o.last_changed_time).to be_nil
      end

      it "initializes without an object" do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        o = Object.new

        # validate results
        expect(o.name).to eq "Beget::Object: #{o.object_id.to_s(16)}"
        expect(o.last_refreshed_time).to be_nil
        expect(o.last_changed_time).to be_nil
      end

      it "initializes from cache" do
        # create test state/variables
        obj = 'Test object'
        cache_str = StringIO.new
        c1 = Cache.new(cache_str)
        o1 = Object.new(obj, name: 'o', cache: c1)
        o1.refresh!
        c2 = Cache.new(StringIO.new(cache_str.string))

        # mocks/stubs/expected calls
        # execute method
        o = Object.new(name: 'o', cache: c2)

        # validate results
        expect(o.data).to eq obj
        expect(o.data_hash).to eq o1.data_hash
        expect(o.last_refreshed_time).to eq o1.last_refreshed_time
        expect(o.last_changed_time).to eq o1.last_changed_time
        expect(o.last_parent_change_time).to eq o1.last_parent_change_time
      end

      it "initializes from cache with uncacheable object" do
        # create test state/variables
        obj = ::File.open("Rakefile.rb")
        cache_str = StringIO.new
        c1 = Cache.new(cache_str)
        o1 = Object.new(obj, name: 'o', cache: c1)
        o1.refresh!
        c2 = Cache.new(StringIO.new(cache_str.string))

        # mocks/stubs/expected calls
        # execute method
        o = Object.new(name: 'o', cache: c2)

        # validate results
        expect(o.data).to_not eq obj
        expect(o.data_hash).to_not eq o1.data_hash
        expect(o.last_refreshed_time).to be_nil
        expect(o.last_changed_time).to be_nil
        expect(o.last_parent_change_time).to be_nil
      end

    end

    describe '#data' do

      it "returns the object" do
        # create test state/variables
        obj = "Random object"
        o = Object.new(obj)

        # mocks/stubs/expected calls
        # execute method
        data = o.data

        # validate results
        expect(data).to be obj
      end

    end

    describe '#refresh!' do

      it "does nothing if no block was specified" do
        # create test state/variables
        obj = "Random object"
        o = Object.new(obj)
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        o.refresh!

        # validate results
        expect(o.last_refreshed_time).to be > prev_time
        expect(o.last_changed_time).to be_nil
      end

      it "executes the update block" do
        # create test state/variables
        obj = "Random object"
        o = Object.new(obj) do |obj, parents_data|
          obj << 's'
        end
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        o.refresh!

        # validate results
        expect(o.data).to eq "Random objects"
        expect(o.last_refreshed_time).to be > prev_time
        expect(o.last_changed_time).to eq o.last_refreshed_time
      end

    end

  end
end
