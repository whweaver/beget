require 'spec_helper'
require 'beget'

module Beget
  describe Node do

    describe '#beget_many!' do

      it "begets a NodeList" do
        # create test state/variables
        n = Node.new

        # mocks/stubs/expected calls
        # execute method
        n.beget_many!(name: 'nl') do
          Node.new(name: 'b')
        end

        # validate results
        expect(n.children.keys).to eq %w[nl]
      end

    end

  end

  describe NodeList do

    describe '#initialize' do

      it "initializes without block" do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        nl = NodeList.new

        # validate results
      end

      it "initializes with block" do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        nl = NodeList.new do
          nl.children.values + [Node.new]
        end

        # validate results
      end

    end

    describe '#beget!' do

      it "calls super when called with an argument" do
        # create test state/variables
        nl = NodeList.new

        # mocks/stubs/expected calls
        # execute method
        nl.beget!(Node.new(name: 'a'))

        # validate results
        expect(nl.children.keys).to eq ['a']
      end

      it "adds a child generation block with no children" do
        # create test state/variables
        nl = NodeList.new

        # mocks/stubs/expected calls
        # execute method
        nl.beget! do
          Node.new
        end

        # validate results
      end

      it "adds a child generation block and executes it for each child" do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        nl = NodeList.new(children: [a, b])

        # mocks/stubs/expected calls
        # execute method
        gc = nl.beget! do |n|
          Node.new(name: "#{n.name}_child")
        end

        # validate results
        expect(a.children.keys).to eq ['a_child']
        expect(b.children.keys).to eq ['b_child']
        expect(gc.children.keys).to eq ['a_child', 'b_child']
      end

      it "adds a nil child generation block and executes it for each child" do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        nl = NodeList.new(children: [a, b])

        # mocks/stubs/expected calls
        # execute method
        gc = nl.beget! do |n|
          nil
        end

        # validate results
        expect(a.children).to eq({})
        expect(b.children).to eq({})
        expect(gc.children).to eq({})
      end

      it "raises an error if a block is not given" do
        # create test state/variables
        nl = NodeList.new

        # mocks/stubs/expected calls
        # execute method
        expect{
          nl.beget!
        }.to raise_error(/A block must be provided/)

        # validate results
      end

    end

    describe '#beget_from!' do

      it "Adds parent to children and calls super" do
        # create test state/variables
        c = Node.new(name: 'c')
        p = Node.new(name: 'p')
        nl = NodeList.new(name: 'nl', children: [c])

        # mocks/stubs/expected calls
        # execute method
        nl.beget_from!(p)

        # validate results
        expect(nl.parents).to eq({'p' => p})
        expect(c.parents).to eq({'nl' => nl, 'p' => p})
      end

    end

    describe '#unbeget!' do

      it "removes parents from child and calls super" do
        # create test state/variables
        c = Node.new(name: 'c')
        p = Node.new(name: 'p')
        nl = NodeList.new(name: 'nl', parents: [p], children: [c])
        expect(nl.children).to eq({'c' => c})
        expect(p.children).to eq({'nl' => nl, 'c' => c})

        # mocks/stubs/expected calls
        # execute method
        nl.unbeget!(c)

        # validate results
        expect(nl.children).to eq({})
        expect(p.children).to eq({'nl' => nl})
      end

    end

    describe '#unbeget_from!' do

      it "removes parents from child and calls super" do
        # create test state/variables
        c = Node.new(name: 'c')
        p = Node.new(name: 'p')
        nl = NodeList.new(name: 'nl', parents: [p], children: [c])
        expect(nl.children).to eq({'c' => c})
        expect(p.children).to eq({'nl' => nl, 'c' => c})

        # mocks/stubs/expected calls
        # execute method
        nl.unbeget_from!(p)

        # validate results
        expect(nl.parents).to eq({})
        expect(c.parents).to eq({'nl' => nl})
      end

    end

    describe '#refresh!' do

      it "Adds new children to the NodeList" do
        # create test state/variables
        count = 0
        nl = NodeList.new do |parents_data|
          count += 1
          nl.children.values + [Node.new(name: "n#{count}")]
        end

        # mocks/stubs/expected calls
        # execute method
        nl.refresh!

        # validate results
        expect(nl.children.keys).to eq ['n1']
      end

      it "Removes old children from the NodeList" do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        nl = NodeList.new(children: [a, b]) do |parents_data|
          [a]
        end

        # mocks/stubs/expected calls
        # execute method
        nl.refresh!

        # validate results
        expect(nl.children).to eq({'a' => a})
      end

      it "Adds new children to the NodeList and adds new grandchildren" do
        # create test state/variables
        count = 0
        nl = NodeList.new do |parents_data|
          count += 1
          nl.children.values + [Node.new(name: "n#{count}")]
        end
        nl.refresh!
        gc = nl.beget! do |n|
          Node.new(name: "#{n.name}_child")
        end
        expect(nl.children.keys).to eq ['n1']
        expect(nl.children['n1'].children.keys).to eq ['n1_child']
        expect(gc.children.keys).to eq ['n1_child']

        # mocks/stubs/expected calls
        # execute method
        nl.refresh!

        # validate results
        expect(nl.children.keys).to eq ['n1', 'n2']
        expect(nl.children['n1'].children.keys).to eq ['n1_child']
        expect(nl.children['n2'].children.keys).to eq ['n2_child']
        expect(gc.children.keys).to eq ['n1_child', 'n2_child']
      end

      it "Removes old children from the NodeList and removes old grandchildren" do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        nl = NodeList.new(children: [a, b]) do |parents_data|
          [a]
        end
        gc = nl.beget! do |n|
          Node.new(name: "#{n.name}_child")
        end
        expect(nl.children.keys).to eq ['a', 'b']
        expect(a.children.keys).to eq ['a_child']
        expect(b.children.keys).to eq ['b_child']
        expect(gc.children.keys).to eq ['a_child', 'b_child']

        # mocks/stubs/expected calls
        # execute method
        nl.refresh!

        # validate results
        expect(nl.children).to eq({'a' => a})
        expect(a.children.keys).to eq ['a_child']
        expect(b.children.keys).to eq ['b_child']
        expect(gc.children.keys).to eq ['a_child']
      end

    end

    describe '#data' do

      it "returns children" do
        # create test state/variables
        a = Node.new(name: 'a')
        nl = NodeList.new(children: [a])

        # mocks/stubs/expected calls
        # execute method
        expect(nl.data).to eq({ 'a' => a })

        # validate results
      end

    end

  end
end
