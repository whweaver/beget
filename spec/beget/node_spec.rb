require 'spec_helper'
require 'beget'
require 'nokogiri'

module Beget
  describe Node do

    describe '#initialize' do

      it 'initializes with no options' do
        # create test state/variables
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        n = Node.new

        # validate results
        expect(n.name).to eq "Beget::Node: #{n.object_id.to_s(16)}"
        expect(n.last_refreshed_time).to be_nil
        expect(n.last_changed_time).to be_nil
        expect(n.start_time).to be > prev_time
        expect(n.start_time).to be < Time.now
        expect(n.parents).to eq({})
        expect(n.children).to eq({})
        expect(n.external?).to be_falsey
        expect(n.executor).to be Concurrent.global_io_executor
      end

      it 'initializes name' do
        # create test state/variables

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(name: 'sample_name')

        # validate results
        expect(n.name).to eq 'sample_name'
      end

      it 'initializes last_refreshed_time' do
        # create test state/variables
        last_refreshed_time = Time.now - 100

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(last_refreshed_time: last_refreshed_time)

        # validate results
        expect(n.last_refreshed_time).to eq last_refreshed_time
      end

      it 'raises error when last_refreshed_time is in the future' do
        # create test state/variables
        last_refreshed_time = Time.now + 100

        # mocks/stubs/expected calls
        # execute method
        expect {
          Node.new(last_refreshed_time: last_refreshed_time)
        }.to raise_error(/Cannot handle times in the future!/)

        # validate results
      end

      it 'initializes last_changed_time' do
        # create test state/variables
        last_changed_time = Time.now - 100

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(last_changed_time: last_changed_time)

        # validate results
        expect(n.last_changed_time).to eq last_changed_time
      end

      it 'raises error when last_changed_time is in the future' do
        # create test state/variables
        last_changed_time = Time.now + 100

        # mocks/stubs/expected calls
        # execute method
        expect {
          Node.new(last_changed_time: last_changed_time)
        }.to raise_error(/Cannot handle times in the future!/)

        # validate results
      end

      it 'initializes last_parent_change_time' do
        # create test state/variables
        last_parent_change_time = Time.now - 100

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(last_parent_change_time: last_parent_change_time)

        # validate results
        expect(n.last_parent_change_time).to eq last_parent_change_time
      end

      it 'raises error when last_parent_change_time is in the future' do
        # create test state/variables
        last_parent_change_time = Time.now + 100

        # mocks/stubs/expected calls
        # execute method
        expect {
          Node.new(last_parent_change_time: last_parent_change_time)
        }.to raise_error(/Cannot handle times in the future!/)

        # validate results
      end

      it 'initialized external' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(external: true)

        # validate results
        expect(n.external?).to be_truthy
      end

      it 'initializes executor' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(executor: Concurrent.global_immediate_executor)

        # validate results
        expect(n.executor).to be Concurrent.global_immediate_executor
      end

      it 'initializes interpret_to_children_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_to_children_as: :node)

        # validate results
        expect(n.interpret_to_children_as).to be :node
      end

      it 'initializes missing interpret_to_children_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new

        # validate results
        expect(n.interpret_to_children_as).to be :data
      end

      it 'initializes interpret_to_self_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_to_self_as: :html)

        # validate results
        expect(n.interpret_to_self_as).to be :html
      end

      it 'initializes missing interpret_to_self_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new

        # validate results
        expect(n.interpret_to_self_as).to be :data
      end

      it 'initializes interpret_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_as: :json)

        # validate results
        expect(n.interpret_to_children_as).to be :json
        expect(n.interpret_to_self_as).to be :json
      end

      it 'initializes interpret_parents_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_parents_as: :marshal)

        # validate results
        expect(n.interpret_parents_as).to be :marshal
      end

      it 'initializes pass_parents_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(pass_parents_as: :hash)

        # validate results
        expect(n.pass_parents_as).to be :hash
      end

      it 'initializes missing pass_parents_as' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new

        # validate results
        expect(n.pass_parents_as).to be :args
      end

      it 'initializes children' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(name: 'n', children: [a, b])

        # validate results
        expect(n.children).to eq({'a' => a, 'b' => b})
        expect(a.parents).to eq({'n' => n})
        expect(b.parents).to eq({'n' => n})
      end

      it 'initializes parents' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(name: 'n', parents: [a, b])

        # validate results
        expect(n.parents).to eq({'a' => a, 'b' => b})
        expect(a.children).to eq({'n' => n})
        expect(b.children).to eq({'n' => n})
      end

      it 'initializes with empty cache' do
        # create test state/variables
        io = StringIO.new
        c = Cache.new(io)

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(name: 'n', cache: c)

        # validate results
        expect(n.data_hash).to eq nil.hash
        expect(n.last_refreshed_time).to be_nil
        expect(n.last_changed_time).to be_nil
        expect(n.last_parent_change_time).to be_nil
      end

      it 'initializes with cached state' do
        # create test state/variables
        io1 = StringIO.new
        c1 = Cache.new(io1)
        n1 = Node.new(name: 'n', cache: c1)
        n1.refresh!
        c2 = Cache.new(StringIO.new(io1.string))

        # mocks/stubs/expected calls
        # execute method
        n2 = Node.new(name: 'n', cache: c2)

        # validate results
        expect(n2.data_hash).to eq n1.data_hash
        expect(n2.last_refreshed_time).to eq n1.last_refreshed_time
        expect(n2.last_changed_time).to eq n1.last_changed_time
        expect(n2.last_parent_change_time).to eq n1.last_parent_change_time
      end

      it 'initializes from file cache' do
        # create test state/variables
        cache_path = ::File.join(TEST_DIRECTORY, 'Node', '.begetcache')
        c1 = Cache.new(cache_path)
        n1 = Node.new(name: 'n', cache: c1)
        n1.refresh!
        c2 = Cache.new(cache_path)

        # mocks/stubs/expected calls
        # execute method
        n2 = Node.new(name: 'n', cache: c2)

        # validate results
        expect(n2.data_hash).to eq n1.data_hash
        expect(n2.last_refreshed_time).to eq n1.last_refreshed_time
        expect(n2.last_changed_time).to eq n1.last_changed_time
        expect(n2.last_parent_change_time).to eq n1.last_parent_change_time
      end

    end

    describe '#name' do

      it 'returns the name' do
        # create test state/variables
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        name = n.name

        # validate results
        expect(name).to eq 'n'
      end

    end

    describe '#last_refreshed_time' do

      it 'returns the last refreshed time' do
        # create test state/variables
        last_refreshed_time = Time.now - 365
        n = Node.new(last_refreshed_time: last_refreshed_time)

        # mocks/stubs/expected calls
        # execute method
        lut = n.last_refreshed_time

        # validate results
        expect(lut).to eq last_refreshed_time
      end
      
    end

    describe '#last_changed_time' do

      it 'returns the last updated time' do
        # create test state/variables
        last_changed_time = Time.now - 365
        n = Node.new(last_changed_time: last_changed_time)

        # mocks/stubs/expected calls
        # execute method
        lct = n.last_changed_time

        # validate results
        expect(lct).to eq last_changed_time
      end
      
    end

    describe '#start_time' do

      it 'returns the start time' do
        # create test state/variables
        prev_time = Time.now
        n = Node.new

        # mocks/stubs/expected calls
        # execute method
        st = n.start_time

        # validate results
        expect(st).to be > prev_time
        expect(st).to be < Time.now
      end

    end

    describe '#last_parent_change_time' do

      it 'returns the last time a parent was added or removed' do
        # create test state/variables
        last_parent_change_time = Time.now - 365
        n = Node.new(last_parent_change_time: last_parent_change_time)

        # mocks/stubs/expected calls
        # execute method
        lpct = n.last_parent_change_time

        # validate results
        expect(lpct).to eq last_parent_change_time
      end
      
    end

    describe '#cache' do

      it 'returns the cache' do
        # create test state/variables
        c = Cache.new(StringIO.new)
        n = Node.new(cache: c)

        # mocks/stubs/expected calls
        # execute method
        cache = n.cache

        # validate results
        expect(cache).to be c
      end
      
    end

    describe '#data_hash' do

      it 'computes the hash when it hasnt been computed yet' do
        # create test state/variables
        n = Node.new

        # mocks/stubs/expected calls
        expect(n).to receive(:compute_hash).and_return(12345)

        # execute method
        dh = n.data_hash

        # validate results
        expect(dh).to eq 12345
      end

      it 'returns the precomputed hash' do
        # create test state/variables
        n = Node.new(data_hash: 67890)

        # mocks/stubs/expected calls
        expect(n).to_not receive(:compute_hash)

        # execute method
        dh = n.data_hash

        # validate results
        expect(dh).to eq 67890
      end
      
    end

    describe '#external?' do

      it 'returns whether or not the node is external' do
        # create test state/variables
        n = Node.new(external: true)

        # mocks/stubs/expected calls
        # execute method
        e = n.external?

        # validate results
        expect(e).to be_truthy
      end

    end

    describe '#executor' do

      it "returns the node's default executor" do
        # create test state/variables
        n = Node.new(executor: Concurrent.global_fast_executor)

        # mocks/stubs/expected calls
        # execute method
        e = n.executor

        # validate results
        expect(e).to be Concurrent.global_fast_executor
      end

    end

    describe '#parents' do

      it 'returns the parents of the node' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(parents: [a, b])

        # validate results
        expect(n.parents).to eq({'a' => a, 'b' => b})
      end

    end

    describe '#children' do

      it 'returns the children of the node' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')

        # mocks/stubs/expected calls
        # execute method
        n = Node.new(children: [a, b])

        # validate results
        expect(n.children).to eq({'a' => a, 'b' => b})
      end

    end

    describe '#interpret_to_children_as' do

      it 'returns how to interpret the node to its children' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_to_children_as: :yaml)

        # validate results
        expect(n.interpret_to_children_as).to eq :yaml
      end

    end

    describe '#interpret_to_self_as' do

      it 'returns how to interpret the node to itself' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_to_self_as: :xml)

        # validate results
        expect(n.interpret_to_self_as).to eq :xml
      end

    end

    describe '#interpret_parents_as' do

      it 'returns how to interpret the parents of this node' do
        # create test state/variables
        # mocks/stubs/expected calls
        # execute method
        n = Node.new(interpret_parents_as: :none)

        # validate results
        expect(n.interpret_parents_as).to eq :none
      end

    end

    describe '#interpret_parent_as' do

      it 'returns how to interpret the given parent with default' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(parents: [a])

        # mocks/stubs/expected calls
        # execute method
        # validate results
        expect(n.interpret_parent_as('a')).to eq :data
      end

      it 'returns how to interpret the given parent with interpret_to_children_as' do
        # create test state/variables
        a = Node.new(name: 'a', interpret_to_children_as: :node)
        n = Node.new(parents: [a])

        # mocks/stubs/expected calls
        # execute method
        # validate results
        expect(n.interpret_parent_as('a')).to eq :node
      end

      it 'returns how to interpret the given parent with interpret_parents_as' do
        # create test state/variables
        a = Node.new(name: 'a', interpret_to_children_as: :node)
        n = Node.new(parents: [a], interpret_parents_as: :json)

        # mocks/stubs/expected calls
        # execute method
        # validate results
        expect(n.interpret_parent_as('a')).to eq :json
      end

      it 'returns how to interpret the given parent with beget interpret_as' do
        # create test state/variables
        a = Node.new(name: 'a', interpret_to_children_as: :node)
        n = Node.new
        a.beget!(n, interpret_as: :xml)

        # mocks/stubs/expected calls
        # execute method
        # validate results
        expect(n.interpret_parent_as('a')).to eq :xml
      end

    end

    describe '#beget!' do

      it 'adds a node as a child' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        n.beget!(a)

        # validate results
        expect(n.children).to eq({'a' => a})
        expect(a.parents).to eq({'n' => n})
      end

      it 'raises error if node already has a child' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n', children: [a])

        # mocks/stubs/expected calls
        # execute method
        expect {
          n.beget!(a)
        }.to raise_error(/Node n already has a child with name a/)

        # validate results
        expect(n.children).to eq({'a' => a})
        expect(a.parents).to eq({'n' => n})
      end

      it 'adds a node with interpretation information' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        n.beget!(a, interpret_as: :marshal)

        # validate results
        expect(n.children).to eq({'a' => a})
        expect(a.parents).to eq({'n' => n})
        expect(a.interpret_parent_as(n.name)).to eq :marshal
      end

    end

    describe '#beget_from!' do

      it 'adds a node as a parent' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        n = Node.new(name: 'n', children: [a])
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        n.beget_from!(b)

        # validate results
        expect(n.children).to eq({'a' => a})
        expect(n.parents).to eq({'b' => b})
        expect(a.parents).to eq({'n' => n})
        expect(a.children).to eq({})
        expect(b.parents).to eq({})
        expect(b.children).to eq({'n' => n})
        expect(n.last_parent_change_time).to be > prev_time
      end

      it 'raises error if node already has a parent' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n', parents: [a])

        # mocks/stubs/expected calls
        # execute method
        expect {
          n.beget_from!(a)
        }.to raise_error(/Node n already has a parent with name a/)

        # validate results
        expect(n.parents).to eq({'a' => a})
        expect(a.children).to eq({'n' => n})
      end

      it 'adds a node with interpretation information' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        n.beget_from!(a, interpret_as: :csv_header)

        # validate results
        expect(a.children).to eq({'n' => n})
        expect(n.parents).to eq({'a' => a})
        expect(n.interpret_parent_as(a.name)).to eq :csv_header
      end

    end

    describe '#unbeget!' do

      it 'removes a child' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        n = Node.new(name: 'n', children: [a], parents: [b])

        # mocks/stubs/expected calls
        # execute method
        n.unbeget!(a)

        # validate results
        expect(n.children).to eq({})
        expect(n.parents).to eq({'b' => b})
        expect(a.parents).to eq({})
        expect(a.children).to eq({})
        expect(b.parents).to eq({})
        expect(b.children).to eq({'n' => n})
      end

      it 'raises error if node does not have a child' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        expect {
          n.unbeget!(a)
        }.to raise_error(/Node n has no child a/)

        # validate results
        expect(n.children).to eq({})
        expect(a.parents).to eq({})
      end

    end

    describe '#unbeget_from!' do

      it 'removes a parent' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        n = Node.new(name: 'n', parents: [b])
        prev_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        n.unbeget_from!(b)

        # validate results
        expect(n.children).to eq({})
        expect(n.parents).to eq({})
        expect(a.parents).to eq({})
        expect(a.children).to eq({})
        expect(b.parents).to eq({})
        expect(b.children).to eq({})
        expect(n.last_parent_change_time).to be > prev_time
      end

      it 'raises error if node does not have a parent' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        expect {
          n.unbeget_from!(a)
        }.to raise_error(/Node n has no parent a/)

        # validate results
        expect(n.parents).to eq({})
        expect(a.children).to eq({})
      end

    end

    describe '#refresh!' do

      it 'passes parents data as :node with :interpret_as' do
        # create test state/variables
        p1 = Node.new(interpret_as: :node)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh_data!).with(nil, p1)

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :none with :interpret_to_children_as' do
        # create test state/variables
        p1 = Node.new(interpret_to_children_as: :none)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh_data!).with(nil)

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :data with :interpret_parents_as' do
        # create test state/variables
        p1 = Node.new
        n = Node.new(interpret_parents_as: :data)
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh_data!).with(nil, nil)

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :string with :interpret_as in beget!' do
        # create test state/variables
        p1 = Node.new
        n = Node.new
        p1.beget!(n, interpret_as: :string)

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh_data!).with(nil, "")

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :html with :interpret_as' do
        # create test state/variables
        str = "<html><body><h1>Hello World!</h1></body></html\n"
        html = Nokogiri::HTML(str)
        p1 = Node.new(interpret_as: :html)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!) do |this_data, parents_data|
          expect(this_data).to be_nil
          expect(parents_data.to_html).to eq html.to_html
        end

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :xml with :interpret_to_children_as' do
        # create test state/variables
        str = "<html><body><h1>Hello World!</h1></body></html\n"
        html = Nokogiri::XML(str)
        p1 = Node.new(interpret_to_children_as: :xml)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!) do |this_data, parents_data|
          expect(this_data).to be_nil
          expect(parents_data.to_xml).to eq html.to_xml
        end

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :json with :interpret_parents_as' do
        # create test state/variables
        str = '{"hello": "world", "number": 4}'
        p1 = Node.new
        n = Node.new(interpret_parents_as: :json)
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, {"hello" => "world", "number" => 4})

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :yaml with :interpret_as in beget!' do
        # create test state/variables
        str = <<-HEREDOC
- something: else
  pi: 3.14
HEREDOC
        p1 = Node.new
        n = Node.new
        p1.beget!(n, interpret_as: :yaml)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, [{"something" => "else", "pi" => 3.14}])

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :csv with :interpret_as' do
        # create test state/variables
        str = "0,1,1,2,3,5,8\none,two,four,nine,sixteen,twenty-five,thirty-six\n0.,0.1,0.2,0.3,.4,0.5,0.6"
        p1 = Node.new(interpret_as: :csv)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, [[0, 1, 1, 2, 3, 5, 8],
                                                        %w[one two four nine sixteen twenty-five thirty-six],
                                                        [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]])

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :csv_header with :interpret_to_children_as' do
        # create test state/variables
        str = "1,2,3,4,5,6,7\n0,1,1,2,3,5,8\n1,2,4,9,16,25,36\n0.,0.1,0.2,0.3,.4,0.5,0.6"
        p1 = Node.new(interpret_to_children_as: :csv_header)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, [{1 => 0, 2 => 1, 3 => 1, 4 => 2, 5 => 3, 6 => 5, 7 => 8},
                                                        {1 => 1, 2 => 2, 3 => 4, 4 => 9, 5 => 16, 6 => 25, 7 => 36},
                                                        {1 => 0.0, 2 => 0.1, 3 => 0.2, 4 => 0.3, 5 => 0.4, 6 => 0.5, 7 => 0.6}])

        # execute method
        n.refresh!

        # validate results
      end

      it 'passes parents data as :marshal with :interpret_parents_as' do
        # create test state/variables
        obj = {"something" => ["to", :marshal]}
        str = Marshal.dump(obj)
        p1 = Node.new
        n = Node.new(interpret_parents_as: :marshal)
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, obj)

        # execute method
        n.refresh!

        # validate results
      end

      it 'raises error when bad value provided to :interpret_as' do
        # create test state/variables
        p1 = Node.new(interpret_as: :bad)
        n = Node.new
        p1.beget!(n)

        # mocks/stubs/expected calls
        # execute method

        expect {
          n.refresh!
        }.to raise_error(/Unknown format 'bad'/)

        # validate results
      end

      it 'passes parents data as a hash with :pass_parents_as' do
        # create test state/variables
        obj = {"something" => ["to", :marshal]}
        str = Marshal.dump(obj)
        p1 = Node.new(interpret_as: :marshal)
        n = Node.new(pass_parents_as: :hash)
        p1.beget!(n)

        # mocks/stubs/expected calls
        expect(p1).to receive(:get_data).and_return(str)
        expect(n).to receive(:refresh_data!).with(nil, {p1.name => obj})

        # execute method
        n.refresh!

        # validate results
      end

    end

    describe '#process!' do

      it 'refreshes if the node is external' do
        # create test state/variables
        n = Node.new(external: true)
        n.refresh! # Make sure to refresh first to actually test the external part
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if the node has never been refreshed' do
        # create test state/variables
        n = Node.new
        prev_refreshed_time = Time.now

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if a refresh is requested by the refresh? method' do
        # create test state/variables
        n = Node.new
        n.refresh!
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh?).and_return(true)

        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if at least one parent was changed more recently than this was refreshed' do
        # create test state/variables
        a = Node.new(name: 'a')
        b = Node.new(name: 'b')
        n = Node.new(name: 'n', parents: [a, b])
        n.refresh!
        prev_refreshed_time = n.last_refreshed_time

        expect(a).to receive(:compute_hash).and_return(Time.now.hash)
        expect(a).to receive(:compute_hash).and_return(Time.now.hash)
        a.refresh!

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if this node has not been refreshed since start and at least one parent has unknown change time' do
        # create test state/variables
        a = Node.new
        n = Node.new(parents: [a], last_refreshed_time: Time.now - 365)
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if a parent has been added since the last refresh' do
        # create test state/variables
        a = Node.new
        n = Node.new
        a.refresh!
        n.refresh!
        a.beget!(n)
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'refreshes if a parent has been removed since the last refresh' do
        # create test state/variables
        a = Node.new
        n = Node.new(parents: [a])
        a.refresh!
        n.refresh!
        a.unbeget!(n)
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > prev_refreshed_time
      end

      it 'does not refresh if everything is up-to-date' do
        # create test state/variables
        a = Node.new
        n = Node.new(parents: [a])
        a.refresh!
        n.refresh!
        prev_refreshed_time = n.last_refreshed_time

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to eq prev_refreshed_time
      end

      it 'processes parent first if child is processed' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n', parents: [a])
        expect(a.last_refreshed_time).to be_nil
        expect(n.last_refreshed_time).to be_nil

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!(parents: true)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be > a.last_refreshed_time
      end

      it 'processes child later if parent is processed' do
        # create test state/variables
        a = Node.new(name: 'a')
        n = Node.new(name: 'n', children: [a])
        expect(a.last_refreshed_time).to be_nil
        expect(n.last_refreshed_time).to be_nil

        # mocks/stubs/expected calls
        expect(n).to receive(:compute_hash).and_return(Time.now.hash)
        expect(n).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = n.process!(children: true)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(n.last_refreshed_time).to be < a.last_refreshed_time
      end

      it 'processes diamond dependency from bottom' do
        # create test state/variables
        gp = Node.new(name: 'gp')
        p1 = Node.new(name: 'p1', parents: [gp])
        p2 = Node.new(name: 'p2', parents: [gp])
        c = Node.new(name: 'c', parents: [p1, p2])

        # mocks/stubs/expected calls
        # execute method
        pc = c.process!(parents: true, children: true)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(p1.last_refreshed_time).to be < c.last_refreshed_time
        expect(p2.last_refreshed_time).to be < c.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p1.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p2.last_refreshed_time
      end

      it 'processes diamond dependency from top' do
        # create test state/variables
        gp = Node.new(name: 'gp')
        p1 = Node.new(name: 'p1', parents: [gp])
        p2 = Node.new(name: 'p2', parents: [gp])
        c = Node.new(name: 'c', parents: [p1, p2])

        # mocks/stubs/expected calls
        expect(gp).to receive(:compute_hash).and_return(Time.now.hash)
        expect(gp).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p1).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p1).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p2).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p2).to receive(:compute_hash).and_return(Time.now.hash)
        expect(c).to receive(:compute_hash).and_return(Time.now.hash)
        expect(c).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = gp.process!(parents: true, children: true)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(p1.last_refreshed_time).to be < c.last_refreshed_time
        expect(p2.last_refreshed_time).to be < c.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p1.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p2.last_refreshed_time
      end

      it 'processes diamond dependency from middle' do
        # create test state/variables
        gp = Node.new(name: 'gp')
        p1 = Node.new(name: 'p1', parents: [gp])
        p2 = Node.new(name: 'p2', parents: [gp])
        c = Node.new(name: 'c', parents: [p1, p2])

        # mocks/stubs/expected calls
        expect(gp).to receive(:compute_hash).and_return(Time.now.hash)
        expect(gp).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p2).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p2).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = p1.process!(family: true)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(p1.last_refreshed_time).to be < c.last_refreshed_time
        expect(p2.last_refreshed_time).to be < c.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p1.last_refreshed_time
        expect(gp.last_refreshed_time).to be < p2.last_refreshed_time
      end

      it 'processes hourglass dependency from the top' do
        # create test state/variables
        gp1 = Node.new(name: 'gp1')
        gp2 = Node.new(name: 'gp2')
        p = Node.new(name: 'p', parents: [gp1, gp2])
        c1 = Node.new(name: 'c1', parents: [p])
        c2 = Node.new(name: 'c2', parents: [p])

        # mocks/stubs/expected calls
        expect(gp1).to receive(:compute_hash).and_return(Time.now.hash)
        expect(gp1).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = gp1.process!(family: true)
        expect(pc.done?).to be_falsey
        pc.wait

        # validate results
        expect(pc.done?).to be_truthy
        expect { pc.raise_rejections }.to_not raise_error
        expect(p.last_refreshed_time).to be < c1.last_refreshed_time
        expect(p.last_refreshed_time).to be < c2.last_refreshed_time
        expect(gp1.last_refreshed_time).to be < p.last_refreshed_time
        expect(gp2.last_refreshed_time).to be < p.last_refreshed_time
      end

      it 'processes hourglass dependency from the middle' do
        # create test state/variables
        gp1 = Node.new(name: 'gp1')
        gp2 = Node.new(name: 'gp2')
        p = Node.new(name: 'p', parents: [gp1, gp2])
        c1 = Node.new(name: 'c1', parents: [p])
        c2 = Node.new(name: 'c2', parents: [p])

        # mocks/stubs/expected calls
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = p.process!(family: true)
        expect(pc.done?).to be_falsey
        pc.wait

        # validate results
        expect(pc.done?).to be_truthy
        expect { pc.raise_rejections }.to_not raise_error
        expect(p.last_refreshed_time).to be < c1.last_refreshed_time
        expect(p.last_refreshed_time).to be < c2.last_refreshed_time
        expect(gp1.last_refreshed_time).to be < p.last_refreshed_time
        expect(gp2.last_refreshed_time).to be < p.last_refreshed_time
      end

      it 'processes hourglass dependency from the bottom' do
        # create test state/variables
        gp1 = Node.new(name: 'gp1')
        gp2 = Node.new(name: 'gp2')
        p = Node.new(name: 'p', parents: [gp1, gp2])
        c1 = Node.new(name: 'c1', parents: [p])
        c2 = Node.new(name: 'c2', parents: [p])

        # mocks/stubs/expected calls
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)
        expect(p).to receive(:compute_hash).and_return(Time.now.hash)

        # execute method
        pc = c2.process!(family: true)
        expect(pc.done?).to be_falsey
        pc.wait

        # validate results
        expect(pc.done?).to be_truthy
        expect { pc.raise_rejections }.to_not raise_error
        expect(p.last_refreshed_time).to be < c1.last_refreshed_time
        expect(p.last_refreshed_time).to be < c2.last_refreshed_time
        expect(gp1.last_refreshed_time).to be < p.last_refreshed_time
        expect(gp2.last_refreshed_time).to be < p.last_refreshed_time
      end

      it 'processes with custom default executor' do
        # create test state/variables
        executor = Concurrent::ThreadPoolExecutor.new
        n = Node.new(name: 'n', executor: executor)

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(executor.completed_task_count).to be > 0

        executor.shutdown
      end

      it 'processes with custom executor' do
        # create test state/variables
        executor = Concurrent::ThreadPoolExecutor.new
        n = Node.new(name: 'n')

        # mocks/stubs/expected calls
        # execute method
        pc = n.process!(executor: executor)
        pc.wait

        # validate results
        expect { pc.raise_rejections }.to_not raise_error
        expect(executor.completed_task_count).to be > 0

        executor.shutdown
      end

      it 'raises exception' do
        # create test state/variables
        n = Node.new(name: 'n', external: true)

        # mocks/stubs/expected calls
        expect(n).to receive(:refresh_data!).and_raise("Example error")

        # execute method
        pc = n.process!
        pc.wait

        # validate results
        expect {
          pc.raise_rejections
        }.to raise_error("Example error")
      end

    end

  end
end
