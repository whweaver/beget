require 'spec_helper'
require 'beget'
require 'active_record'

module Beget

  ::ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'tmp/test.db')

  class CreateTestRecordTable < ::ActiveRecord::Migration[5.2]
    def up
      unless ::ActiveRecord::Base.connection.table_exists?(:test_records)
        create_table :test_records do |table|
          table.string :name
          table.integer :value
          table.timestamps
        end
      end
    end

    def down
      if ::ActiveRecord::Base.connection.table_exists?(:test_records)
        drop_table :test_records
      end
    end
  end

  CreateTestRecordTable.migrate(:down)
  CreateTestRecordTable.migrate(:up)

  class TestRecord < ::ActiveRecord::Base
  end

  describe Node do

    describe '#beget_active_record!' do

      it "begets an ActiveRecord" do
        # create test state/variables
        tr = TestRecord.new do |tr|
          tr.name = "test_name"
          tr.value = 486
        end
        n = Node.new

        # mocks/stubs/expected calls
        # execute method
        n.beget_active_record!(tr, name: 'ar')

        # validate results
        expect(n.children.keys).to eq %w[ar]
      end

    end

  end

  describe ActiveRecord do

    describe '#initialize' do

      it "initializes with an unsaved record" do
        # create test state/variables
        tr = TestRecord.new do |tr|
          tr.name = "test_name"
          tr.value = 486
        end

        # mocks/stubs/expected calls
        # execute method
        n = ActiveRecord.new(tr)

        # validate results
        expect(n.name).to eq "Beget::ActiveRecord::Beget::TestRecord: "
      end

      it "initializes with saved record" do
        # create test state/variables
        tr = TestRecord.new do |tr|
          tr.name = "test_name"
          tr.value = 486
        end
        tr.save

        # mocks/stubs/expected calls
        # execute method
        n = ActiveRecord.new(tr)

        # validate results
        expect(n.name).to match /Beget::ActiveRecord::Beget::TestRecord: \d+/
      end

    end

    describe '#refresh!' do

      it "refreshes record" do
        # create test state/variables
        tr = TestRecord.new do |tr|
          tr.name = "test_name"
          tr.value = 486
        end
        tr.save
        n = ActiveRecord.new(tr) do |tr|
          tr.name = "new_name"
          tr.value = 487
          tr.save
        end
        expect(tr.name).to eq "test_name"
        expect(tr.value).to eq 486
        prev_hash = n.data_hash

        # mocks/stubs/expected calls
        # execute method
        n.refresh!

        # validate results
        expect(n.name).to match /Beget::ActiveRecord::Beget::TestRecord: \d+/
        expect(tr.name).to eq "new_name"
        expect(tr.value).to eq 487
        expect(n.data_hash).to_not eq prev_hash
      end

    end

    describe '#data_as!' do

      it "raises error if string is requested" do
        # create test state/variables
        tr = TestRecord.new do |tr|
          tr.name = "test_name"
          tr.value = 486
        end
        tr.save
        n = ActiveRecord.new(tr, interpret_as: :string) do |tr|
          tr.name = "new_name"
          tr.value = 487
          tr.save
        end

        # mocks/stubs/expected calls
        # execute method
        expect {
          n.refresh!
        }.to raise_error(/Cannot convert ActiveRecord to string!/)

        # validate results
      end

    end

  end
end
